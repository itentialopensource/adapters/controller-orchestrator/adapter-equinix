# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Equinix System. The API that was used to build the adapter for Equinix is usually available in the report directory of this adapter. The adapter utilizes the Equinix API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Equinix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Equinix Network Edge.

With this adapter you have the ability to perform operations with Equinix on items such as:

- VPN
- BGP
- Layer 2
- Devices

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
