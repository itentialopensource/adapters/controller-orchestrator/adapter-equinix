/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-equinix',
      type: 'Equinix',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Equinix = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Equinix Adapter Test', () => {
  describe('Equinix Class Tests', () => {
    const a = new Equinix(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const setupSendAgreementUsingPOST1BodyParam = {
      accountNumber: 'string',
      apttusId: 'string'
    };
    describe('#sendAgreementUsingPOST1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sendAgreementUsingPOST1(setupSendAgreementUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'sendAgreementUsingPOST1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const setupSendDnsLookupPOST1BodyParam = {
      fqdns: [
        'velocloud.net'
      ],
      uuid: '4cfb5675-5c3f-4275-adba-0c9e3c26c96b',
      metroCode: 'SY'
    };
    describe('#sendDnsLookupPOST1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sendDnsLookupPOST1(setupSendDnsLookupPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.additionalProp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'sendDnsLookupPOST1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const setupPostPublicKeyUsingPOSTBodyParam = {
      accountUcmId: 678967,
      keyName: 'myKeyName',
      keyValue: 'myKeyValue'
    };
    describe('#postPublicKeyUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPublicKeyUsingPOST(setupPostPublicKeyUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'postPublicKeyUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const setupMetro = 'fakedata';
    describe('#getAccountsWithStatusUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsWithStatusUsingGET(setupMetro, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.accountCreateUrl);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.totalCount);
                assert.equal('string', data.response.errorMessage);
                assert.equal('string', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getAccountsWithStatusUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const setupAccountNumber = 'fakedata';
    describe('#getAgreementStatusUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAgreementStatusUsingGET(setupAccountNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.errorMessage);
                assert.equal('string', data.response.isValid);
                assert.equal('string', data.response.terms);
                assert.equal('string', data.response.termsVersionID);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getAgreementStatusUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrderTermsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrderTermsUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.terms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getOrderTermsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const setupVendorPackage = 'fakedata';
    const setupLicenseType = 'fakedata';
    describe('#getVendorTermsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVendorTermsUsingGET(setupVendorPackage, setupLicenseType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.terms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getVendorTermsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualDevicesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualDevicesUsingGET(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.pagination);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getVirtualDevicesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetrosUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetrosUsingGET(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal('object', typeof data.response.pagination);
                assert.equal(1, data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getMetrosUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrderSummaryUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrderSummaryUsingGET(setupAccountNumber, setupMetro, setupVendorPackage, setupLicenseType, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.accountNumber);
                assert.equal('string', data.response.agreementId);
                assert.equal(true, Array.isArray(data.response.charges));
                assert.equal('string', data.response.currency);
                assert.equal('string', data.response.errorCode);
                assert.equal('string', data.response.errorMessage);
                assert.equal('string', data.response.esignAgreementId);
                assert.equal('string', data.response.ibxCountry);
                assert.equal('string', data.response.ibxRegion);
                assert.equal(1, data.response.initialTerm);
                assert.equal('string', data.response.metro);
                assert.equal(5, data.response.monthlyRecurringCharges);
                assert.equal(5, data.response.nonRecurringCharges);
                assert.equal('string', data.response.nonRenewalNotice);
                assert.equal('string', data.response.orderTerms);
                assert.equal('string', data.response.piPercentage);
                assert.equal('string', data.response.productDescription);
                assert.equal(7, data.response.quantity);
                assert.equal('string', data.response.quoteContentType);
                assert.equal('string', data.response.quoteFileName);
                assert.equal('string', data.response.referenceId);
                assert.equal(10, data.response.renewalPeriod);
                assert.equal('string', data.response.requestSignType);
                assert.equal('string', data.response.signStatus);
                assert.equal('string', data.response.signType);
                assert.equal('string', data.response.speed);
                assert.equal('string', data.response.status);
                assert.equal(7, data.response.totalCharges);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getOrderSummaryUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievePriceUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievePriceUsingGET(setupAccountNumber, setupMetro, setupVendorPackage, setupLicenseType, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.primary);
                assert.equal('object', typeof data.response.secondary);
                assert.equal('24', data.response.termLength);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'retrievePriceUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicKeysUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPublicKeysUsingGET(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Setup', 'getPublicKeysUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let virtualDeviceLicensingUuid = 'fakedata';
    const virtualDeviceLicensingDraft = true;
    const virtualDeviceLicensingCreateVirtualDeviceUsingPOSTBodyParam = {
      deviceTypeCode: 'CSR1000V',
      hostNamePrefix: 'mySR',
      licenseMode: 'SUB',
      metroCode: 'SV',
      version: '16.09.03',
      virtualDeviceName: 'Router1-csr1000v',
      notifications: [
        'test1@equinix.com',
        'test2@equinix.com'
      ],
      deviceManagementType: 'EQUINIX-CONFIGURED',
      core: 4
    };
    describe('#createVirtualDeviceUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVirtualDeviceUsingPOST(virtualDeviceLicensingCreateVirtualDeviceUsingPOSTBodyParam, virtualDeviceLicensingDraft, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('877a3aa2-c49a-4af1-98a6-007424e737ae', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              virtualDeviceLicensingUuid = data.response.uuid;
              saveMockData('VirtualDeviceLicensing', 'createVirtualDeviceUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualDeviceLicensingFile = 'fakedata';
    const virtualDeviceLicensingMetroCode = 'fakedata';
    const virtualDeviceLicensingDeviceTypeCode = 'fakedata';
    const virtualDeviceLicensingLicenseType = 'fakedata';
    describe('#uploadLicenseUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadLicenseUsingPOST(virtualDeviceLicensingFile, virtualDeviceLicensingMetroCode, virtualDeviceLicensingDeviceTypeCode, virtualDeviceLicensingLicenseType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fileId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'uploadLicenseUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadLicenseForDeviceUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadLicenseForDeviceUsingPOST(virtualDeviceLicensingUuid, virtualDeviceLicensingFile, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fileId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'uploadLicenseForDeviceUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualDevicesUsingGET1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualDevicesUsingGET1(null, null, virtualDeviceLicensingMetroCode, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pagination);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'getVirtualDevicesUsingGET1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualDeviceLicensingUpdateVirtualDeviceUsingPUTBodyParam = {
      deviceTypeCode: 'CSR1000V',
      hostNamePrefix: 'mySR',
      licenseMode: 'SUB',
      metroCode: 'SV',
      version: '16.09.03',
      virtualDeviceName: 'Router1-csr1000v',
      notifications: [
        'test1@equinix.com',
        'test2@equinix.com'
      ],
      deviceManagementType: 'EQUINIX-CONFIGURED',
      core: 4
    };
    describe('#updateVirtualDeviceUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateVirtualDeviceUsingPUT(virtualDeviceLicensingDraft, virtualDeviceLicensingUpdateVirtualDeviceUsingPUTBodyParam, virtualDeviceLicensingUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'updateVirtualDeviceUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualDeviceLicensingUpdateVirtualDeviceUsingPATCH1BodyParam = {
      notifications: [
        'test1@example.com'
      ],
      termLength: 4,
      virtualDeviceName: 'RCiscoSTROY',
      clusterName: 'myCluster0123',
      status: 'PROVISIONED'
    };
    describe('#updateVirtualDeviceUsingPATCH1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVirtualDeviceUsingPATCH1(virtualDeviceLicensingUuid, virtualDeviceLicensingUpdateVirtualDeviceUsingPATCH1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'updateVirtualDeviceUsingPATCH1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualDeviceUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualDeviceUsingGET(virtualDeviceLicensingUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ABC INC', data.response.accountName);
                assert.equal('133911', data.response.accountNumber);
                assert.equal('cust0001', data.response.createdBy);
                assert.equal('2018-01-30T10:30:31.387Z', data.response.createdDate);
                assert.equal('53791666484', data.response.deviceSerialNo);
                assert.equal('ROUTER', data.response.deviceTypeCategory);
                assert.equal('CSR1000V', data.response.deviceTypeCode);
                assert.equal('CSR 1000v', data.response.deviceTypeName);
                assert.equal('object', typeof data.response.expiry);
                assert.equal('AMER', data.response.region);
                assert.equal('Cisco', data.response.deviceTypeVendor);
                assert.equal('VR-SV-CSR1000V-cust0001-1', data.response.hostName);
                assert.equal('877a3aa2-c49a-4af1-98a6-007424e737ae', data.response.uuid);
                assert.equal('cust0002', data.response.lastUpdatedBy);
                assert.equal('2018-01-30T10:30:31.387Z', data.response.lastUpdatedDate);
                assert.equal('877a3aa2-c49a-4af1-98a6-007424e737ae', data.response.licenseFileId);
                assert.equal('Bring your own license', data.response.licenseName);
                assert.equal('REGISTERED', data.response.licenseStatus);
                assert.equal('BYOL', data.response.licenseType);
                assert.equal('SV', data.response.metroCode);
                assert.equal('Silicon Valley', data.response.metroName);
                assert.equal('zone1', data.response.zoneCode);
                assert.equal('Zone 1', data.response.zoneName);
                assert.equal('AWS-Azure-Router-csr1000v', data.response.name);
                assert.equal(true, Array.isArray(data.response.notifications));
                assert.equal('SEC', data.response.packageCode);
                assert.equal('Security', data.response.packageName);
                assert.equal('16.09.02', data.response.version);
                assert.equal('PO1223', data.response.purchaseOrderNumber);
                assert.equal('PRIMARY', data.response.redundancyType);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa67xx', data.response.redundantUuid);
                assert.equal('10.195.11.23', data.response.sshIpAddress);
                assert.equal('test-device-168-201-97-149.eis.lab.equinix.com', data.response.sshIpFqdn);
                assert.equal('PROVISIONED', data.response.status);
                assert.equal(500, data.response.throughput);
                assert.equal('Mbps', data.response.throughputUnit);
                assert.equal('49289456-a63e-47d4-927c-5161cfb77501', data.response.aclTemplateUuid);
                assert.equal('object', typeof data.response.core);
                assert.equal('object', typeof data.response.pricingDetails);
                assert.equal(10, data.response.interfaceCount);
                assert.equal('EQUINIX-CONFIGURED', data.response.deviceManagementType);
                assert.equal('object', typeof data.response.userPublicKey);
                assert.equal('10.195.237.228/26', data.response.managementIp);
                assert.equal('10.195.237.254', data.response.managementGatewayIp);
                assert.equal('149.97.198.95/31', data.response.publicIp);
                assert.equal('149.97.198.94', data.response.publicGatewayIp);
                assert.equal('4.0.0.53', data.response.primaryDnsName);
                assert.equal('129.250.35.250', data.response.secondaryDnsName);
                assert.equal(12, data.response.termLength);
                assert.equal(200, data.response.additionalBandwidth);
                assert.equal('12345', data.response.siteId);
                assert.equal('192.168.2.5', data.response.systemIpAddress);
                assert.equal('object', typeof data.response.vendorConfig);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal(1029, data.response.asn);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'getVirtualDeviceUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualDeviceLicensingUpdateAdditionalBandwidthBodyParam = {
      additionalBandwidth: 100
    };
    describe('#updateAdditionalBandwidth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAdditionalBandwidth(virtualDeviceLicensingUuid, virtualDeviceLicensingUpdateAdditionalBandwidthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'updateAdditionalBandwidth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualDeviceInterfacesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualDeviceInterfacesUsingGET(virtualDeviceLicensingUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'getVirtualDeviceInterfacesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualDeviceLicensingUpdateLicenseUsingPOSTBodyParam = {
      token: 'A1025025'
    };
    describe('#updateLicenseUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateLicenseUsingPOST(virtualDeviceLicensingUuid, virtualDeviceLicensingUpdateLicenseUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'updateLicenseUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pingDeviceUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pingDeviceUsingGET(virtualDeviceLicensingUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'pingDeviceUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let aCLTemplateUuid = 'fakedata';
    const aCLTemplateCreateDeviceACLTemplateUsingPostBodyParam = {
      name: 'Test Template',
      description: 'Test Template Description',
      metroCode: 'DC',
      inBoundRules: [
        {
          srcType: 'DOMAIN',
          protocol: 'DNS',
          srcPort: 53,
          dstPort: 'any',
          seqNo: 1
        }
      ]
    };
    describe('#createDeviceACLTemplateUsingPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceACLTemplateUsingPost(aCLTemplateCreateDeviceACLTemplateUsingPostBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('877a3aa2-c49a-4af1-98a6-007424e737ae', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              aCLTemplateUuid = data.response.uuid;
              saveMockData('ACLTemplate', 'createDeviceACLTemplateUsingPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceACLTemplateUsingGET1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceACLTemplateUsingGET1(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pagination);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACLTemplate', 'getDeviceACLTemplateUsingGET1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aCLTemplateUpdateDeviceACLTemplateUsingPUTBodyParam = {
      name: 'Test Template',
      description: 'Test Template Description',
      metroCode: 'DC',
      inBoundRules: [
        {
          srcType: 'DOMAIN',
          protocol: 'DNS',
          srcPort: 53,
          dstPort: 'any',
          seqNo: 1
        }
      ]
    };
    describe('#updateDeviceACLTemplateUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceACLTemplateUsingPUT(aCLTemplateUuid, null, aCLTemplateUpdateDeviceACLTemplateUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACLTemplate', 'updateDeviceACLTemplateUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTemplatebyUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTemplatebyUuid(aCLTemplateUuid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('New template', data.response.name);
                assert.equal('be7ef79e-31e7-4769-be5b-e192496f48aa', data.response.uuid);
                assert.equal('My description', data.response.description);
                assert.equal(true, Array.isArray(data.response.inBoundRules));
                assert.equal('SV', data.response.metroCode);
                assert.equal('Silicon Valley', data.response.metroName);
                assert.equal('Test Device', data.response.virtualDeviceName);
                assert.equal('ce7ef79e-31e7-4769-be5b-e192496f48ab', data.response.virtualDeviceUuid);
                assert.equal('PROVISIONING', data.response.deviceACLstatus);
                assert.equal('nfvsit01', data.response.createdBy);
                assert.equal('2020/10/03T19:41:17.976Z', data.response.createdDate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACLTemplate', 'getDeviceTemplatebyUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deviceLinkUuid = 'fakedata';
    const deviceLinkCreateLinkGroupUsingPOSTBodyParam = {
      groupName: 'linkGroup',
      subnet: '192.164.0.0/29'
    };
    describe('#createLinkGroupUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLinkGroupUsingPOST(deviceLinkCreateLinkGroupUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('a5d9182d-c529-451d-b137-3742d5a742ce', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              deviceLinkUuid = data.response.uuid;
              saveMockData('DeviceLink', 'createLinkGroupUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinkGroupsUsingGET1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLinkGroupsUsingGET1(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLink', 'getLinkGroupsUsingGET1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceLinkUpdateLinkGroupUsingPATCHBodyParam = {
      groupName: 'linkGroup',
      subnet: '192.164.0.0/29'
    };
    describe('#updateLinkGroupUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateLinkGroupUsingPATCH(deviceLinkUuid, deviceLinkUpdateLinkGroupUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLink', 'updateLinkGroupUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinkGroupByUUIDUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLinkGroupByUUIDUsingGET(deviceLinkUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Samsung', data.response.accountName);
                assert.equal('nfv-sit1', data.response.createdBy);
                assert.equal('2019-09-15T10:30:31.387Z', data.response.createdDate);
                assert.equal('vik-chain', data.response.groupName);
                assert.equal(1130, data.response.id);
                assert.equal('reg2-acc1', data.response.lastUpdatedBy);
                assert.equal('2019-09-16T10:30:31.387Z', data.response.lastUpdatedDate);
                assert.equal(true, Array.isArray(data.response.metroDevices));
                assert.equal('API', data.response.source);
                assert.equal('PROVISIONING', data.response.status);
                assert.equal('10.0.0.0/27', data.response.subnet);
                assert.equal('6ea5a0e4-2bf7-45c2-9aa7-e846a8cd5567', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLink', 'getLinkGroupByUUIDUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const layer2ConnectionsServiceProfilesCreateConnectionUsingPOSTBodyParam = {
      primaryName: 'v3-api-test-pri',
      virtualDeviceUuid: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      profileUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      authorizationKey: 444111000222,
      speed: 50,
      speedUnit: 'MB',
      notifications: [
        'sandboxuser@example-company.com'
      ],
      purchaseOrderNumber: '312456323',
      sellerMetroCode: 'SV',
      interfaceId: 5,
      secondaryName: 'v3-api-test-sec1',
      namedTag: 'Private',
      secondaryVirtualDeviceUuid: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      secondaryProfileUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      secondaryAuthorizationKey: 444111000222,
      secondarySellerMetroCode: 'SV',
      secondarySpeed: 50,
      secondarySpeedUnit: 'MB',
      secondaryNotifications: [
        'sandboxuser@example-company.com'
      ],
      secondaryInterfaceId: 6,
      primaryZSideVlanCTag: 101,
      secondaryZSideVlanCTag: 102,
      primaryZSidePortUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      primaryZSideVlanSTag: 301,
      secondaryZSidePortUUID: 'xxxxx192-xx70-xxxx-xx04-xxxxxxxa37xx',
      secondaryZSideVlanSTag: 302
    };
    describe('#createConnectionUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConnectionUsingPOST(layer2ConnectionsServiceProfilesCreateConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Connection created successfully', data.response.message);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.primaryConnectionId);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.secondaryConnectionId);
                assert.equal('SUCCESS', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'createConnectionUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const layer2ConnectionsServiceProfilesAuthorizationKey = 'fakedata';
    const layer2ConnectionsServiceProfilesMetroCode = 'fakedata';
    const layer2ConnectionsServiceProfilesProfileId = 'fakedata';
    const layer2ConnectionsServiceProfilesRegion = 'fakedata';
    describe('#validateAuthorizationKeyUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateAuthorizationKeyUsingGET(layer2ConnectionsServiceProfilesAuthorizationKey, layer2ConnectionsServiceProfilesMetroCode, layer2ConnectionsServiceProfilesProfileId, layer2ConnectionsServiceProfilesRegion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Authorization key provided is valid', data.response.message);
                assert.equal('VALID', data.response.status);
                assert.equal('object', typeof data.response.primary);
                assert.equal('object', typeof data.response.secondary);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'validateAuthorizationKeyUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const layer2ConnectionsServiceProfilesAction = 'fakedata';
    const layer2ConnectionsServiceProfilesUuid = 'fakedata';
    const layer2ConnectionsServiceProfilesPerformUserActionUsingPATCHBodyParam = {
      accessKey: samProps.authentication.password,
      secretKey: samProps.authentication.password
    };
    describe('#performUserActionUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.performUserActionUsingPATCH(layer2ConnectionsServiceProfilesAction, layer2ConnectionsServiceProfilesUuid, layer2ConnectionsServiceProfilesPerformUserActionUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'performUserActionUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectionByUuidUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnectionByUuidUsingGET(layer2ConnectionsServiceProfilesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Forsythe Solutions Group, Inc.', data.response.buyerOrganizationName);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.uuid);
                assert.equal('Test-123', data.response.name);
                assert.equal(1015, data.response.vlanSTag);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.portUUID);
                assert.equal('TEST-CH2-CX-SEC-01', data.response.portName);
                assert.equal('dot1q', data.response.asideEncapsulation);
                assert.equal('CH', data.response.metroCode);
                assert.equal('Chicago', data.response.metroDescription);
                assert.equal('PROVISIONED', data.response.providerStatus);
                assert.equal('PROVISIONED', data.response.status);
                assert.equal('Up to 500MB', data.response.billingTier);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.authorizationKey);
                assert.equal(500, data.response.speed);
                assert.equal('MB', data.response.speedUnit);
                assert.equal('secondary', data.response.redundancyType);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.redundancyGroup);
                assert.equal('CH', data.response.sellerMetroCode);
                assert.equal('Chicago', data.response.sellerMetroDescription);
                assert.equal('XYZ Cloud Service', data.response.sellerServiceName);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.sellerServiceUUID);
                assert.equal('EQUINIX-CLOUD-EXCHANGE', data.response.sellerOrganizationName);
                assert.equal(true, Array.isArray(data.response.notifications));
                assert.equal('O-1234567890', data.response.purchaseOrderNumber);
                assert.equal('Private', data.response.namedTag);
                assert.equal('2017-09-26T22:46:24.312Z', data.response.createdDate);
                assert.equal('sandboxuser@example-company.com', data.response.createdBy);
                assert.equal('Sandbox User', data.response.createdByFullName);
                assert.equal('sandboxuser@example-company.com', data.response.createdByEmail);
                assert.equal('sandboxuser@example-company.com', data.response.lastUpdatedBy);
                assert.equal('2017-09-26T23:01:46Z', data.response.lastUpdatedDate);
                assert.equal('Sandbox User', data.response.lastUpdatedByFullName);
                assert.equal('sandboxuser@example-company.com', data.response.lastUpdatedByEmail);
                assert.equal('TEST-CHG-06GMR-Tes-2-TES-C', data.response.zSidePortName);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.zSidePortUUID);
                assert.equal(515, data.response.zSideVlanCTag);
                assert.equal(2, data.response.zSideVlanSTag);
                assert.equal(true, data.response.remote);
                assert.equal('false,', data.response.private);
                assert.equal('false,', data.response.self);
                assert.equal('xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx', data.response.redundantUuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'getConnectionByUuidUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfilesByMetroUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProfilesByMetroUsingGET(layer2ConnectionsServiceProfilesMetroCode, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isLastPage);
                assert.equal(55, data.response.totalCount);
                assert.equal(true, data.response.isFirstPage);
                assert.equal(1000, data.response.pageSize);
                assert.equal(1, data.response.pageNumber);
                assert.equal(true, Array.isArray(data.response.content));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'getProfilesByMetroUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileByIdUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProfileByIdUsingGET(layer2ConnectionsServiceProfilesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isLastPage);
                assert.equal(55, data.response.totalCount);
                assert.equal(true, data.response.isFirstPage);
                assert.equal(1000, data.response.pageSize);
                assert.equal(1, data.response.pageNumber);
                assert.equal(true, Array.isArray(data.response.content));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'getProfileByIdUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let bGPUuid = 'fakedata';
    const bGPAddBgpConfigurationUsingPOSTBodyParam = {
      authenticationKey: 'pass123',
      connectionUuid: 'f79eead8-b837-41d3-9095-9b15c2c4996d',
      localAsn: 10012,
      localIpAddress: '100.210.1.221/30',
      remoteAsn: 10013,
      remoteIpAddress: '100.210.1.31'
    };
    describe('#addBgpConfigurationUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBgpConfigurationUsingPOST(bGPAddBgpConfigurationUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              bGPUuid = data.response.uuid;
              saveMockData('BGP', 'addBgpConfigurationUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBgpConfigurationsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBgpConfigurationsUsingGET(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.authenticationKey);
                assert.equal('string', data.response.connectionUuid);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.createdByEmail);
                assert.equal('string', data.response.createdByFullName);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.deletedBy);
                assert.equal('string', data.response.deletedByEmail);
                assert.equal('string', data.response.deletedByFullName);
                assert.equal('string', data.response.deletedDate);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.lastUpdatedByEmail);
                assert.equal('string', data.response.lastUpdatedByFullName);
                assert.equal('string', data.response.lastUpdatedDate);
                assert.equal(8, data.response.localAsn);
                assert.equal('string', data.response.localIpAddress);
                assert.equal('string', data.response.provisioningStatus);
                assert.equal(4, data.response.remoteAsn);
                assert.equal('string', data.response.remoteIpAddress);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.virtualDeviceUuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'getBgpConfigurationsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bGPUpdateBgpConfigurationUsingPUTBodyParam = {
      authenticationKey: 'pass123',
      localAsn: 10012,
      localIpAddress: '100.210.1.221/30',
      remoteAsn: 10013,
      remoteIpAddress: '100.210.1.31'
    };
    describe('#updateBgpConfigurationUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateBgpConfigurationUsingPUT(bGPUuid, bGPUpdateBgpConfigurationUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'updateBgpConfigurationUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBgpConfigurationUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBgpConfigurationUsingGET(bGPUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.authenticationKey);
                assert.equal('string', data.response.connectionUuid);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.createdByEmail);
                assert.equal('string', data.response.createdByFullName);
                assert.equal('string', data.response.createdDate);
                assert.equal('string', data.response.deletedBy);
                assert.equal('string', data.response.deletedByEmail);
                assert.equal('string', data.response.deletedByFullName);
                assert.equal('string', data.response.deletedDate);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.lastUpdatedByEmail);
                assert.equal('string', data.response.lastUpdatedByFullName);
                assert.equal('string', data.response.lastUpdatedDate);
                assert.equal(7, data.response.localAsn);
                assert.equal('string', data.response.localIpAddress);
                assert.equal('string', data.response.provisioningStatus);
                assert.equal(3, data.response.remoteAsn);
                assert.equal('string', data.response.remoteIpAddress);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.virtualDeviceUuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BGP', 'getBgpConfigurationUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vPNCreateVpnUsingPOSTBodyParam = {
      siteName: 'Chicago',
      virtualDeviceUuid: 'f79eead8-b837-41d3-9095-9b15c2c4996d',
      peerIp: '110.11.12.222',
      peerSharedKey: '5bb2424e888bd',
      remoteAsn: 65413,
      remoteIpAddress: '100.210.1.31',
      password: samProps.authentication.password,
      tunnelIp: '192.168.7.2/30'
    };
    describe('#createVpnUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createVpnUsingPOST(vPNCreateVpnUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'createVpnUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnsUsingGET(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pagination);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'getVpnsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vPNUuid = 'fakedata';
    const vPNUpdateVpnConfigurationUsingPutBodyParam = {
      siteName: 'Chicago',
      virtualDeviceUuid: 'f79eead8-b837-41d3-9095-9b15c2c4996d',
      peerIp: '110.11.12.222',
      peerSharedKey: '5bb2424e888bd',
      remoteAsn: 65413,
      remoteIpAddress: '100.210.1.31',
      password: samProps.authentication.password,
      tunnelIp: '192.168.7.2/30'
    };
    describe('#updateVpnConfigurationUsingPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateVpnConfigurationUsingPut(vPNUuid, vPNUpdateVpnConfigurationUsingPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'updateVpnConfigurationUsingPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVpnByUuidUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVpnByUuidUsingGET(vPNUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Chicago', data.response.siteName);
                assert.equal('877a3aa2-c49a-4af1-98a6-007424e737ae', data.response.uuid);
                assert.equal('f79eead8-b837-41d3-9095-9b15c2c4996d', data.response.virtualDeviceUuid);
                assert.equal('Traffic from AWS cloud', data.response.configName);
                assert.equal('PROVISIONED', data.response.status);
                assert.equal('110.11.12.222', data.response.peerIp);
                assert.equal('5bb2424e888bd', data.response.peerSharedKey);
                assert.equal(65413, data.response.remoteAsn);
                assert.equal('100.210.1.31', data.response.remoteIpAddress);
                assert.equal('pass123', data.response.password);
                assert.equal(65414, data.response.localAsn);
                assert.equal('192.168.7.2/30', data.response.tunnelIp);
                assert.equal('ESTABLISHED', data.response.bgpState);
                assert.equal('8780', data.response.inboundBytes);
                assert.equal('8780', data.response.inboundPackets);
                assert.equal('8765', data.response.outboundBytes);
                assert.equal('8765', data.response.outboundPackets);
                assert.equal('UP', data.response.tunnelStatus);
                assert.equal(65555, data.response.custOrgId);
                assert.equal('2018-05-18 06:34:26', data.response.createdDate);
                assert.equal('John', data.response.createdByFirstName);
                assert.equal('Smith', data.response.createdByLastName);
                assert.equal('alpha@beta.com', data.response.createdByEmail);
                assert.equal(123, data.response.createdByUserKey);
                assert.equal(456, data.response.createdByAccountUcmId);
                assert.equal('jsmith', data.response.createdByUserName);
                assert.equal(7863, data.response.createdByCustOrgId);
                assert.equal('My Awesome Org', data.response.createdByCustOrgName);
                assert.equal('ACTIVATED', data.response.createdByUserStatus);
                assert.equal('My Awesome Company', data.response.createdByCompanyName);
                assert.equal('2018-07-21 05:20:20', data.response.lastUpdatedDate);
                assert.equal('John', data.response.updatedByFirstName);
                assert.equal('Smith', data.response.updatedByLastName);
                assert.equal('alpha@beta.com', data.response.updatedByEmail);
                assert.equal(123, data.response.updatedByUserKey);
                assert.equal(456, data.response.updatedByAccountUcmId);
                assert.equal('jsmith', data.response.updatedByUserName);
                assert.equal(7863, data.response.updatedByCustOrgId);
                assert.equal('My Awesome Org', data.response.updatedByCustOrgName);
                assert.equal('ACTIVATED', data.response.updatedByUserStatus);
                assert.equal('My Awesome Company', data.response.updatedByCompanyName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'getVpnByUuidUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sSHUserUuid = 'fakedata';
    let sSHUserDeviceUuid = 'fakedata';
    const sSHUserCreateSshUserUsingPOSTBodyParam = {
      username: 'user1',
      password: samProps.authentication.password,
      deviceUuid: '3da0a663-20d9-4b8f-8c5d-d5cf706840c8'
    };
    describe('#createSshUserUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSshUserUsingPOST(sSHUserCreateSshUserUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('12828472-e6e9-4f2b-98f7-b79cf0fab4ff', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              sSHUserUuid = data.response.uuid;
              sSHUserDeviceUuid = data.response.uuid;
              saveMockData('SSHUser', 'createSshUserUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSHUserSshUserUuid = 'fakedata';
    describe('#associateDeviceUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateDeviceUsingPOST(sSHUserSshUserUuid, sSHUserDeviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHUser', 'associateDeviceUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSHUserUsername = 'fakedata';
    describe('#getSshUsersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSshUsersUsingGET(sSHUserUsername, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pagination);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHUser', 'getSshUsersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#isSshUserAvailableForCreationUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.isSshUserAvailableForCreationUsingGET(sSHUserUsername, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHUser', 'isSshUserAvailableForCreationUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSHUserUpdateSshUserUsingPUTBodyParam = {
      password: samProps.authentication.password
    };
    describe('#updateSshUserUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSshUserUsingPUT(sSHUserUuid, sSHUserUpdateSshUserUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHUser', 'updateSshUserUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshUserUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSshUserUsingGET(sSHUserUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('The unique Id of the ssh user.', data.response.uuid);
                assert.equal('The user name of the ssh user.', data.response.username);
                assert.equal(true, Array.isArray(data.response.deviceUuids));
                assert.equal('object', typeof data.response.metroStatusMap);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHUser', 'getSshUserUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualDeviceLicensingDeleteVRouterUsingDELETEBodyParam = {
      deactivationKey: '8dfbd5ba3610234d9e550032603cc34762af140533e2c1de0111d3451d16eefd',
      secondary: {
        deactivationKey: '8dfbd5ba3610234d9e550032603cc34762af140533e2c1de0111d3451d16eefd'
      }
    };
    describe('#deleteVRouterUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVRouterUsingDELETE(virtualDeviceLicensingUuid, virtualDeviceLicensingDeleteVRouterUsingDELETEBodyParam, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VirtualDeviceLicensing', 'deleteVRouterUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeviceACLUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletedeviceACLUsingDELETE(aCLTemplateUuid, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ACLTemplate', 'deletedeviceACLUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLinkGroupUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLinkGroupUsingDELETE(deviceLinkUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceLink', 'deleteLinkGroupUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectionUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConnectionUsingDELETE(layer2ConnectionsServiceProfilesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2ConnectionsServiceProfiles', 'deleteConnectionUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeVpnConfigurationUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeVpnConfigurationUsingDELETE(vPNUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'removeVpnConfigurationUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dissociateDeviceUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dissociateDeviceUsingDELETE(sSHUserSshUserUuid, sSHUserDeviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSHUser', 'dissociateDeviceUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerVersion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServerVersion((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'getServerVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkHealth((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'checkHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorize((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authorize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reAuth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reAuth((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'reAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refresh - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refresh((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'refresh', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#terminate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.terminate((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'terminate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationSelectAccountBodyParam = {
      acct_id: 'string'
    };
    describe('#selectAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.selectAccount(authenticationSelectAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'selectAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlock2F - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unlock2F((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'unlock2F', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityObjects(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'getSecurityObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsGenerateSecurityObjectBodyParam = {
      name: 'string',
      obj_type: 'OPAQUE'
    };
    describe('#generateSecurityObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateSecurityObject(securityObjectsGenerateSecurityObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.key_size);
                assert.equal('NistP256', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('FortanixHSM', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(true, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'generateSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsImportSecurityObjectBodyParam = {
      name: 'string',
      obj_type: 'OPAQUE'
    };
    describe('#importSecurityObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importSecurityObject(securityObjectsImportSecurityObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'importSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityObject('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.key_size);
                assert.equal('NistP384', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('External', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(false, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'getSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityObject('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'deleteSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsUpdateSecurityObjectBodyParam = {
      name: 'string',
      obj_type: 'OPAQUE'
    };
    describe('#updateSecurityObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSecurityObject('fakedata', securityObjectsUpdateSecurityObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'updateSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsPersistSecurityObjectBodyParam = {
      name: 'string',
      transient_key: 'string'
    };
    describe('#persistSecurityObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.persistSecurityObject(securityObjectsPersistSecurityObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.key_size);
                assert.equal('NistP192', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('FortanixHSM', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(false, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'persistSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrivateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePrivateKey('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'deletePrivateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsRotateSecurityObjectBodyParam = {
      name: 'string',
      obj_type: 'OPAQUE'
    };
    describe('#rotateSecurityObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rotateSecurityObject(securityObjectsRotateSecurityObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.key_size);
                assert.equal('X25519', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('External', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(false, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'rotateSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsGetSecurityObjectValueExBodyParam = {
      kid: 'string',
      name: 'string',
      transient_key: 'string'
    };
    describe('#getSecurityObjectValueEx - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityObjectValueEx(securityObjectsGetSecurityObjectValueExBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.key_size);
                assert.equal('NistP192', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('External', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(true, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'getSecurityObjectValueEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsGetSecurityObjectDigestBodyParam = {
      key: {},
      alg: 'SHA3_384'
    };
    describe('#getSecurityObjectDigest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityObjectDigest(securityObjectsGetSecurityObjectDigestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.digest);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'getSecurityObjectDigest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityObjectValue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityObjectValue('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(2, data.response.key_size);
                assert.equal('NistP521', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('FortanixHSM', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(true, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'getSecurityObjectValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateSecurityObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateSecurityObject('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'activateSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsRevokeSecurityObjectBodyParam = {
      code: 'CessationOfOperation'
    };
    describe('#revokeSecurityObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeSecurityObject('fakedata', securityObjectsRevokeSecurityObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'revokeSecurityObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsDeriveKeyBodyParam = {
      name: 'string',
      key_size: 9,
      key_type: 'OPAQUE',
      mechanism: {}
    };
    describe('#deriveKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deriveKey('fakedata', securityObjectsDeriveKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.key_size);
                assert.equal('NistP384', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('External', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.enabled);
                assert.equal(true, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(false, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'deriveKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsDeriveKeyExBodyParam = {
      key: {
        name: 'string'
      },
      name: 'string',
      key_size: 2,
      key_type: 'OPAQUE',
      mechanism: {}
    };
    describe('#deriveKeyEx - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deriveKeyEx(securityObjectsDeriveKeyExBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.key_size);
                assert.equal('X448', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('FortanixHSM', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(true, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'deriveKeyEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityObjectsAgreeKeyBodyParam = {
      private_key: {
        name: 'string'
      },
      public_key: {
        name: 'string'
      },
      name: 'string',
      key_size: 3,
      key_type: 'OPAQUE',
      mechanism: 'diffie_hellman'
    };
    describe('#agreeKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.agreeKey(securityObjectsAgreeKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.key_size);
                assert.equal('Ed25519', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('FortanixHSM', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(true, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityObjects', 'agreeKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptBodyParam = {
      alg: 'OPAQUE',
      plain: 'string'
    };
    describe('#encrypt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encrypt('fakedata', encryptionAndDecryptionEncryptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encrypt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptExBodyParam = {
      key: {},
      alg: 'OPAQUE',
      plain: 'string'
    };
    describe('#encryptEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptEx(encryptionAndDecryptionEncryptExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionBatchEncryptBodyParam = [
      {
        kid: 'string',
        request: {
          alg: 'OPAQUE',
          plain: 'string',
          mode: 'CBCNOPAD',
          iv: 'string',
          ad: 'string',
          tag_len: 8
        }
      }
    ];
    describe('#batchEncrypt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.batchEncrypt(encryptionAndDecryptionBatchEncryptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'batchEncrypt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptInitBodyParam = {
      alg: 'OPAQUE'
    };
    describe('#encryptInit - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptInit('fakedata', encryptionAndDecryptionEncryptInitBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptInit', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptInitExBodyParam = {
      key: {},
      alg: 'OPAQUE'
    };
    describe('#encryptInitEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptInitEx(encryptionAndDecryptionEncryptInitExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptInitEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptUpdateBodyParam = {
      plain: 'string',
      state: 'string'
    };
    describe('#encryptUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptUpdate('fakedata', encryptionAndDecryptionEncryptUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptUpdateExBodyParam = {
      key: {},
      plain: 'string',
      state: 'string'
    };
    describe('#encryptUpdateEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptUpdateEx(encryptionAndDecryptionEncryptUpdateExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptUpdateEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptFinalBodyParam = {
      state: 'string'
    };
    describe('#encryptFinal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptFinal('fakedata', encryptionAndDecryptionEncryptFinalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptFinal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionEncryptFinalExBodyParam = {
      key: {},
      state: 'string'
    };
    describe('#encryptFinalEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.encryptFinalEx(encryptionAndDecryptionEncryptFinalExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'encryptFinalEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptBodyParam = {
      cipher: 'string'
    };
    describe('#decrypt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decrypt('fakedata', encryptionAndDecryptionDecryptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decrypt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptExBodyParam = {
      key: {},
      cipher: 'string'
    };
    describe('#decryptEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptEx(encryptionAndDecryptionDecryptExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionBatchDecryptBodyParam = [
      {
        kid: 'string',
        request: {
          alg: 'OPAQUE',
          cipher: 'string',
          mode: 'KWP',
          iv: 'string',
          ad: 'string',
          tag: 'string'
        }
      }
    ];
    describe('#batchDecrypt - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.batchDecrypt(encryptionAndDecryptionBatchDecryptBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'batchDecrypt', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptInitBodyParam = {
      alg: 'OPAQUE',
      mode: 'CTR',
      iv: 'string',
      ad: 'string'
    };
    describe('#decryptInit - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptInit('fakedata', encryptionAndDecryptionDecryptInitBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptInit', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptInitExBodyParam = {
      key: {}
    };
    describe('#decryptInitEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptInitEx(encryptionAndDecryptionDecryptInitExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptInitEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptUpdateBodyParam = {
      cipher: 'string',
      state: 'string'
    };
    describe('#decryptUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptUpdate('fakedata', encryptionAndDecryptionDecryptUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptUpdateExBodyParam = {
      key: {},
      cipher: 'string',
      state: 'string'
    };
    describe('#decryptUpdateEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptUpdateEx(encryptionAndDecryptionDecryptUpdateExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptUpdateEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptFinalBodyParam = {
      state: 'string'
    };
    describe('#decryptFinal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptFinal('fakedata', encryptionAndDecryptionDecryptFinalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptFinal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const encryptionAndDecryptionDecryptFinalExBodyParam = {
      key: {},
      state: 'string'
    };
    describe('#decryptFinalEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decryptFinalEx(encryptionAndDecryptionDecryptFinalExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EncryptionAndDecryption', 'decryptFinalEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const signAndVerifySignBodyParam = {
      hash_alg: 'SHA1'
    };
    describe('#sign - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sign('fakedata', signAndVerifySignBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignAndVerify', 'sign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const signAndVerifySignExBodyParam = {
      key: {},
      hash_alg: 'Streebog512'
    };
    describe('#signEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signEx(signAndVerifySignExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignAndVerify', 'signEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const signAndVerifyBatchSignBodyParam = [
      {
        key: {
          kid: 'string',
          name: 'string',
          transient_key: 'string'
        },
        hash_alg: 'RIPEMD160',
        hash: 'string',
        data: 'string',
        mode: {
          PKCS1_V15: {},
          PSS: {
            mgf: {
              mgf1: {
                hash: 'SHA3_224'
              }
            }
          }
        }
      }
    ];
    describe('#batchSign - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.batchSign(signAndVerifyBatchSignBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignAndVerify', 'batchSign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const signAndVerifyVerifyBodyParam = {
      hash_alg: 'SHA3_224',
      signature: 'string'
    };
    describe('#verify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verify('fakedata', signAndVerifyVerifyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignAndVerify', 'verify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const signAndVerifyVerifyExBodyParam = {
      key: {},
      hash_alg: 'Streebog256',
      signature: 'string'
    };
    describe('#verifyEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyEx(signAndVerifyVerifyExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignAndVerify', 'verifyEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const signAndVerifyBatchVerifyBodyParam = [
      {
        key: {
          kid: 'string',
          name: 'string',
          transient_key: 'string'
        },
        hash_alg: 'Blake2b512',
        hash: 'string',
        data: 'string',
        signature: 'string',
        mode: {
          PKCS1_V15: {},
          PSS: {
            mgf: {
              mgf1: {
                hash: 'Blake2b256'
              }
            }
          }
        }
      }
    ];
    describe('#batchVerify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.batchVerify(signAndVerifyBatchVerifyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignAndVerify', 'batchVerify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const digestComputeDigestBodyParam = {
      alg: 'Blake2b384',
      data: 'string'
    };
    describe('#computeDigest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.computeDigest(digestComputeDigestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Digest', 'computeDigest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const digestComputeMacBodyParam = {
      data: 'string'
    };
    describe('#computeMac - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.computeMac('fakedata', digestComputeMacBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Digest', 'computeMac', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const digestComputeMacExBodyParam = {
      key: {},
      data: 'string'
    };
    describe('#computeMacEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.computeMacEx(digestComputeMacExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Digest', 'computeMacEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const digestVerifyMacBodyParam = {
      alg: 'Streebog256',
      data: 'string'
    };
    describe('#verifyMac - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyMac('fakedata', digestVerifyMacBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Digest', 'verifyMac', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const digestVerifyMacExBodyParam = {
      key: {},
      alg: 'Ssl3',
      data: 'string'
    };
    describe('#verifyMacEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyMacEx(digestVerifyMacExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Digest', 'verifyMacEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wrappingAndUnwrappingWrapKeyBodyParam = {
      alg: 'OPAQUE',
      kid: 'string'
    };
    describe('#wrapKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.wrapKey('fakedata', wrappingAndUnwrappingWrapKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WrappingAndUnwrapping', 'wrapKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wrappingAndUnwrappingWrapKeyExBodyParam = {
      key: {},
      subject: {},
      alg: 'OPAQUE'
    };
    describe('#wrapKeyEx - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.wrapKeyEx(wrappingAndUnwrappingWrapKeyExBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WrappingAndUnwrapping', 'wrapKeyEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wrappingAndUnwrappingUnwrapKeyBodyParam = {
      alg: 'OPAQUE',
      obj_type: 'OPAQUE',
      wrapped_key: 'string',
      name: 'string'
    };
    describe('#unwrapKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unwrapKey('fakedata', wrappingAndUnwrappingUnwrapKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.key_size);
                assert.equal('SecP256K1', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('FortanixHSM', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(true, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WrappingAndUnwrapping', 'unwrapKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wrappingAndUnwrappingUnwrapKeyExBodyParam = {
      key: {
        name: 'string'
      },
      alg: 'OPAQUE',
      obj_type: 'OPAQUE',
      wrapped_key: 'string',
      name: 'string'
    };
    describe('#unwrapKeyEx - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unwrapKeyEx(wrappingAndUnwrappingUnwrapKeyExBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.key_size);
                assert.equal('X25519', data.response.elliptic_curve);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.group_id);
                assert.equal('object', typeof data.response.creator);
                assert.equal('string', data.response.kid);
                assert.equal('OPAQUE', data.response.obj_type);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('object', typeof data.response.custom_metadata);
                assert.equal('External', data.response.origin);
                assert.equal('string', data.response.pub_key);
                assert.equal('string', data.response.value);
                assert.equal(false, data.response.enabled);
                assert.equal(false, data.response.compliant_with_policies);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.lastused_at);
                assert.equal('string', data.response.transient_key);
                assert.equal(false, data.response.never_exportable);
                assert.equal('object', typeof data.response.rsa);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WrappingAndUnwrapping', 'unwrapKeyEx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApps(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'getApps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appsCreateAppBodyParam = {
      name: 'string',
      add_groups: {},
      default_group: 'string'
    };
    describe('#createApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApp(appsCreateAppBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'createApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApp('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'getApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApp('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'deleteApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appsUpdateAppBodyParam = {
      name: 'string',
      add_groups: {},
      default_group: 'string'
    };
    describe('#updateApp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateApp('fakedata', appsUpdateAppBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'updateApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appsRegenerateApiKeyBodyParam = {
      secret_size: 2
    };
    describe('#regenerateApiKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regenerateApiKey('fakedata', appsRegenerateApiKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'regenerateApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCredential - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCredential('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apps', 'getCredential', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'getGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsCreateGroupBodyParam = {
      name: 'string'
    };
    describe('#createGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createGroup(groupsCreateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'createGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'getGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deleteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsUpdateGroupBodyParam = {
      name: 'string'
    };
    describe('#updateGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGroup('fakedata', groupsUpdateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'updateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccounts((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'getAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountsCreateAccountBodyParam = {
      name: 'string',
      description: 'string',
      organization: 'string',
      country: 'string',
      phone: 'string',
      notification_pref: 'Phone',
      auth_config: {
        password: {
          require_2fa: false,
          administrators_only: true
        },
        saml: 'string',
        signed_jwt: {
          valid_issuers: [
            'string'
          ],
          signing_keys: {
            kind: 'Stored',
            keys: {},
            url: 'string',
            cache_duration: 10
          }
        }
      },
      add_logging_configs: [
        {
          splunk: {
            host: 'string',
            port: 2,
            index: 'string',
            token: 'string',
            tls: {
              mode: {},
              validate_hostname: true,
              ca: {}
            },
            enabled: false
          },
          stackdriver: {
            log_id: 'string',
            service_account_key: {
              type: 'string',
              project_id: 'string',
              private_key_id: 'string',
              private_key: 'string',
              client_email: 'string'
            },
            enabled: true
          }
        }
      ],
      mod_logging_configs: {},
      del_logging_configs: [
        'string'
      ],
      pending_subscription_change_request: {
        subscription: {},
        contact: 'string',
        comment: 'string'
      },
      enabled: true,
      subscription: {},
      custom_metadata: {}
    };
    describe('#createAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAccount(accountsCreateAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'createAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccount('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'getAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccount('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'deleteAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountsUpdateAccountBodyParam = {
      name: 'string',
      description: 'string',
      organization: 'string',
      country: 'string',
      phone: 'string',
      notification_pref: 'None',
      auth_config: {
        password: {
          require_2fa: true,
          administrators_only: false
        },
        saml: 'string',
        signed_jwt: {
          valid_issuers: [
            'string'
          ],
          signing_keys: {
            kind: 'Fetched',
            keys: {},
            url: 'string',
            cache_duration: 5
          }
        }
      },
      add_logging_configs: [
        {
          splunk: {
            host: 'string',
            port: 8,
            index: 'string',
            token: 'string',
            tls: {
              mode: {},
              validate_hostname: false,
              ca: {}
            },
            enabled: true
          },
          stackdriver: {
            log_id: 'string',
            service_account_key: {
              type: 'string',
              project_id: 'string',
              private_key_id: 'string',
              private_key: 'string',
              client_email: 'string'
            },
            enabled: false
          }
        }
      ],
      mod_logging_configs: {},
      del_logging_configs: [
        'string'
      ],
      pending_subscription_change_request: {
        subscription: {},
        contact: 'string',
        comment: 'string'
      },
      enabled: false,
      subscription: {},
      custom_metadata: {}
    };
    describe('#updateAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccount('fakedata', accountsUpdateAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'updateAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const childAccountsCreateChildAccountBodyParam = {
      name: 'string',
      description: 'string',
      organization: 'string',
      country: 'string',
      phone: 'string',
      notification_pref: 'Both',
      auth_config: {
        password: {
          require_2fa: false,
          administrators_only: true
        },
        saml: 'string',
        signed_jwt: {
          valid_issuers: [
            'string'
          ],
          signing_keys: {
            kind: 'Stored',
            keys: {},
            url: 'string',
            cache_duration: 5
          }
        }
      },
      add_logging_configs: [
        {
          splunk: {
            host: 'string',
            port: 10,
            index: 'string',
            token: 'string',
            tls: {
              mode: {},
              validate_hostname: false,
              ca: {}
            },
            enabled: false
          },
          stackdriver: {
            log_id: 'string',
            service_account_key: {
              type: 'string',
              project_id: 'string',
              private_key_id: 'string',
              private_key: 'string',
              client_email: 'string'
            },
            enabled: false
          }
        }
      ],
      mod_logging_configs: {},
      del_logging_configs: [
        'string'
      ],
      pending_subscription_change_request: {
        subscription: {},
        contact: 'string',
        comment: 'string'
      },
      enabled: true,
      subscription: {},
      custom_metadata: {}
    };
    describe('#createChildAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createChildAccount(childAccountsCreateChildAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccounts', 'createChildAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChildAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getChildAccounts('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccounts', 'getChildAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUsers(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserAccount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUserAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersCreateUserBodyParam = {
      user_email: 'string',
      user_password: 'string',
      recaptcha_response: 'string'
    };
    describe('#createUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUser(usersCreateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'createUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersInviteUserBodyParam = {
      user_email: 'string',
      user_password: 'string'
    };
    describe('#inviteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inviteUser(usersInviteUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'inviteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUser('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserBodyParam = {
      user_email: 'string',
      user_password: 'string'
    };
    describe('#updateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUser('fakedata', usersUpdateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersChangePasswordBodyParam = {
      current_password: 'string',
      new_password: 'string'
    };
    describe('#changePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePassword(usersChangePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'changePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserAccount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersForgotPasswordBodyParam = {
      user_email: 'string'
    };
    describe('#forgotPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.forgotPassword(usersForgotPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'forgotPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersResetPasswordBodyParam = {
      reset_token: 'string',
      new_password: 'string'
    };
    describe('#resetPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetPassword('fakedata', usersResetPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'resetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersProcessInvitationsBodyParam = {
      accepts: [
        'string'
      ],
      rejects: [
        'string'
      ]
    };
    describe('#processInvitations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.processInvitations(usersProcessInvitationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'processInvitations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resendInvitation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resendInvitation('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'resendInvitation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersValidatePasswordResetTokenBodyParam = {
      reset_token: 'string'
    };
    describe('#validatePasswordResetToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.validatePasswordResetToken('fakedata', usersValidatePasswordResetTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'validatePasswordResetToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersConfirmEmailBodyParam = {
      confirm_token: 'string'
    };
    describe('#confirmEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.confirmEmail('fakedata', usersConfirmEmailBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'confirmEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resendConfirmEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resendConfirmEmail('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'resendConfirmEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const childAccountUsersCreateChildAccountUserBodyParam = {
      child_acct_id: 'string',
      user_email: 'string'
    };
    describe('#createChildAccountUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createChildAccountUser(childAccountUsersCreateChildAccountUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccountUsers', 'createChildAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChildAccountUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getChildAccountUser('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccountUsers', 'getChildAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const childAccountUsersUpdateChildAccountUserBodyParam = {
      child_acct_id: 'string',
      user_email: 'string'
    };
    describe('#updateChildAccountUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateChildAccountUser('fakedata', childAccountUsersUpdateChildAccountUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccountUsers', 'updateChildAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteChildAccountUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteChildAccountUser('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccountUsers', 'deleteChildAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllChildAccountUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllChildAccountUsers('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChildAccountUsers', 'getAllChildAccountUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditLogs(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Logs', 'getAuditLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGroupsStats(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getGroupsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppsStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppsStats(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getAppsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppStats('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getAppStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGroupStats('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getGroupStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityObjectStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSecurityObjectStats('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getSecurityObjectStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeU2F - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeU2F((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwoFactorAuthentication', 'authorizeU2F', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeRecoveryCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeRecoveryCode((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwoFactorAuthentication', 'authorizeRecoveryCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateU2FChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateU2FChallenge((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwoFactorAuthentication', 'generateU2FChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lock2F - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lock2F((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwoFactorAuthentication', 'lock2F', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateRecoveryCodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateRecoveryCodes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TwoFactorAuthentication', 'generateRecoveryCodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlugins - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlugins(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getPlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsCreatePluginBodyParam = {
      name: 'string',
      add_groups: [
        'string'
      ],
      default_group: 'string',
      source: {}
    };
    describe('#createPlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPlugin(pluginsCreatePluginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'createPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPlugin('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePlugin('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'deletePlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsUpdatePluginBodyParam = {
      name: 'string',
      add_groups: [
        'string'
      ],
      default_group: 'string',
      source: {}
    };
    describe('#updatePlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePlugin('fakedata', pluginsUpdatePluginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsInvokePluginBodyParam = {};
    describe('#invokePlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.invokePlugin('fakedata', pluginsInvokePluginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'invokePlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysV1PluginsInvokePluginId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSysV1PluginsInvokePluginId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getSysV1PluginsInvokePluginId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApprovalRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApprovalRequests(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'getApprovalRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const approvalRequestsCreateApprovalRequestBodyParam = {
      operation: 'string'
    };
    describe('#createApprovalRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApprovalRequest(approvalRequestsCreateApprovalRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.request_id);
                assert.equal('object', typeof data.response.requester);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.operation);
                assert.equal('string', data.response.method);
                assert.equal('object', typeof data.response.body);
                assert.equal(true, Array.isArray(data.response.approvers));
                assert.equal('object', typeof data.response.denier);
                assert.equal('PENDING', data.response.status);
                assert.equal(true, Array.isArray(data.response.reviewers));
                assert.equal(true, Array.isArray(data.response.subjects));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.expiry);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'createApprovalRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApprovalRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApprovalRequest('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.request_id);
                assert.equal('object', typeof data.response.requester);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.acct_id);
                assert.equal('string', data.response.operation);
                assert.equal('string', data.response.method);
                assert.equal('object', typeof data.response.body);
                assert.equal(true, Array.isArray(data.response.approvers));
                assert.equal('object', typeof data.response.denier);
                assert.equal('DENIED', data.response.status);
                assert.equal(true, Array.isArray(data.response.reviewers));
                assert.equal(true, Array.isArray(data.response.subjects));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.expiry);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'getApprovalRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApprovalRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApprovalRequest('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'deleteApprovalRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approve - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.approve('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'approve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deny - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deny('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'deny', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResult - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getResult('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApprovalRequests', 'getResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortInfoUsingGET2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortInfoUsingGET2((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'getPortInfoUsingGET2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStatUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPostStatUsingGET('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1000000000, data.response.capacity);
                assert.equal(0.002355751, data.response.inboundMax);
                assert.equal(0.0006338511, data.response.outboundMax);
                assert.equal(0.00033159385, data.response.outboundAvg);
                assert.equal(0.0011289178, data.response.inboundLastPolled);
                assert.equal(0.00030398, data.response.outboundLastPolled);
                assert.equal(true, Array.isArray(data.response.inboundStats));
                assert.equal(true, Array.isArray(data.response.outboundStats));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'getPostStatUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllBuyerConnectionsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllBuyerConnectionsUsingGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10000, data.response.pageSize);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(true, data.response.isFirstPage);
                assert.equal(true, data.response.isLastPage);
                assert.equal(5, data.response.pageNumber);
                assert.equal(9, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2Connections', 'getAllBuyerConnectionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const layer2ConnectionsCreateConnectionUsingPOSTBodyParam = {
      primaryName: 'v3-api-test-pri',
      primaryPortUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      primaryVlanSTag: 601,
      primaryVlanCTag: 602,
      profileUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      authorizationKey: 444111000222,
      speed: 50,
      speedUnit: 'MB',
      notifications: [
        'sandboxuser@example-company.com'
      ],
      purchaseOrderNumber: '312456323',
      sellerRegion: 'us-west-1',
      sellerMetroCode: 'SV',
      secondaryName: 'v3-api-test-sec1',
      secondaryPortUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      secondaryVlanSTag: 501,
      secondaryVlanCTag: 501,
      namedTag: 'Private',
      primaryZSideVlanCTag: 101,
      secondaryZSideVlanCTag: 102,
      primaryZSidePortUUID: 'xxxxx191-xx70-xxxx-xx04-xxxxxxxa37xx',
      primaryZSideVlanSTag: 301,
      secondaryZSidePortUUID: 'xxxxx192-xx70-xxxx-xx04-xxxxxxxa37xx',
      secondaryZSideVlanSTag: 302
    };
    const layer2ConnectionsPerformUserActionUsingPATCHBodyParam = {
      accessKey: samProps.authentication.password,
      secretKey: samProps.authentication.password
    };
    describe('#getL2PricesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL2PricesUsingGET('fakedata', null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('USD', data.response.currency);
                assert.equal('SV', data.response.destinationMetro);
                assert.equal('SV', data.response.originMetro);
                assert.equal(true, Array.isArray(data.response.priceList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Layer2Connections', 'getL2PricesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBuyerPreferenceUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBuyerPreferenceUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(20, data.response.portThreshold);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BandwidthAlertPreferences', 'getBuyerPreferenceUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bandwidthAlertPreferencesSaveBuyerPreferenceUsingPOSTBodyParam = {
      emails: [
        'sandboxuser@example-company.com'
      ],
      portThreshold: 20
    };
    describe('#saveBuyerPreferenceUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saveBuyerPreferenceUsingPOST(bandwidthAlertPreferencesSaveBuyerPreferenceUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(20, data.response.portThreshold);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BandwidthAlertPreferences', 'saveBuyerPreferenceUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bandwidthAlertPreferencesUpdateBuyerPreferenceUsingPUTBodyParam = {
      emails: [
        'sandboxuser@example-company.com'
      ],
      portThreshold: 20
    };
    describe('#updateBuyerPreferenceUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateBuyerPreferenceUsingPUT(bandwidthAlertPreferencesUpdateBuyerPreferenceUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BandwidthAlertPreferences', 'updateBuyerPreferenceUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBuyerPreferenceUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBuyerPreferenceUsingDELETE((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BandwidthAlertPreferences', 'deleteBuyerPreferenceUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSellerProfileUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSellerProfileUsingGET(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(20, data.response.pageSize);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(true, data.response.isLastPage);
                assert.equal(2, data.response.totalCount);
                assert.equal(true, data.response.isFirstPage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceProfiles', 'getAllSellerProfileUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceProfilesCreateServiceProfileUsingPOSTBodyParam = {
      additionalBuyerInfo: [
        {
          captureInEmail: true,
          datatype: 'STRING',
          description: 'custBGPAssn description',
          mandatory: true,
          name: 'custBGPAssn'
        }
      ],
      alertPercentage: 10,
      allowCustomSpeed: false,
      apiAvailable: true,
      authKeyLabel: 'Authorization Key',
      connectionAccessibility: 'HYBRID',
      connectionNameLabel: 'Connection Names',
      ctagLabel: 'C-Tag',
      description: 'this profile is test profile example',
      equinixManagedPortAndVlan: true,
      features: {
        cloudReach: true,
        testProfile: true
      },
      integrationId: '34254',
      namedTags: [
        'string'
      ],
      ports: [
        {
          crossConnectId: 'xyz:crossConnect:ws36onof3dbxv4bfywjy52u',
          id: '5E89B4A3FxxxxxxxxxxxxACC88C08A35',
          inTrail: false,
          metroCode: 'SV',
          sellerRegion: 'us-east-1',
          sellerRegionDescription: 'Added for AWS',
          xa: {}
        }
      ],
      private: true,
      requiredRedundancy: true,
      speedBands: [
        {
          speed: 2,
          unit: 'MB'
        }
      ],
      speedFromAPI: false,
      tagType: 'BOTH',
      uuid: 'xxxxx02e-c2f9-xxxx-xxxx-9fba9e7xxxxx',
      status: 'string',
      vlanSameAsPrimary: true,
      name: 'ServiceProfileNoDuplicateName',
      onBandwidthThresholdNotification: [
        'sandboxuser@examplecompany.com'
      ],
      onProfileApprovalRejectNotification: [
        'sandboxuser@examplecompany.com'
      ],
      onVcApprovalRejectionNotification: [
        'test@test.com'
      ],
      privateUserEmails: [
        'sandboxuser@examplecompany.com'
      ]
    };
    describe('#createServiceProfileUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createServiceProfileUsingPOST(serviceProfilesCreateServiceProfileUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceProfiles', 'createServiceProfileUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceProfilesUpdateL2ServiceProfileUsingPUTBodyParam = {
      additionalBuyerInfo: [
        {
          captureInEmail: true,
          datatype: 'STRING',
          description: 'custBGPAssn description',
          mandatory: true,
          name: 'custBGPAssn'
        }
      ],
      alertPercentage: 10,
      allowCustomSpeed: true,
      apiAvailable: true,
      authKeyLabel: 'Authorization Key',
      connectionAccessibility: 'HYBRID',
      connectionNameLabel: 'Connection Names',
      ctagLabel: 'C-Tag',
      description: 'this profile is test profile example',
      equinixManagedPortAndVlan: true,
      features: {
        cloudReach: true,
        testProfile: true
      },
      integrationId: '34254',
      namedTags: [
        'string'
      ],
      ports: [
        {
          crossConnectId: 'xyz:crossConnect:ws36onof3dbxv4bfywjy52u',
          id: '5E89B4A3FxxxxxxxxxxxxACC88C08A35',
          inTrail: true,
          metroCode: 'SV',
          sellerRegion: 'us-east-1',
          sellerRegionDescription: 'Added for AWS',
          xa: {}
        }
      ],
      private: true,
      requiredRedundancy: false,
      speedBands: [
        {
          speed: 10,
          unit: 'MB'
        }
      ],
      speedFromAPI: false,
      tagType: 'BOTH',
      uuid: 'xxxxx02e-c2f9-xxxx-xxxx-9fba9e7xxxxx',
      status: 'string',
      vlanSameAsPrimary: false,
      name: 'ServiceProfileNoDuplicateName',
      onBandwidthThresholdNotification: [
        'sandboxuser@examplecompany.com'
      ],
      onProfileApprovalRejectNotification: [
        'sandboxuser@examplecompany.com'
      ],
      onVcApprovalRejectionNotification: [
        'test@test.com'
      ],
      privateUserEmails: [
        'sandboxuser@examplecompany.com'
      ]
    };
    describe('#updateL2ServiceProfileUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL2ServiceProfileUsingPUT(serviceProfilesUpdateL2ServiceProfileUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceProfiles', 'updateL2ServiceProfileUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateIntegrationIdUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateIntegrationIdUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Example company API integration', data.response.message);
                assert.equal(true, Array.isArray(data.response.metadata));
                assert.equal('SINGLE', data.response.redundancy);
                assert.equal(true, Array.isArray(data.response.sellerRegions));
                assert.equal('VALID', data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceProfiles', 'validateIntegrationIdUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProfileByProfileNameOrUUIDUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteProfileByProfileNameOrUUIDUsingDELETE('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceProfiles', 'deleteProfileByProfileNameOrUUIDUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileByIdOrNameUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProfileByIdOrNameUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('xxxxxxxx-cdaa-44e7-bd36-xxxxxxxxxxxx', data.response.uuid);
                assert.equal('Oracle Cloud Infrastructure -OCI- FastConnect', data.response.name);
                assert.equal(true, data.response.requiredRedundancy);
                assert.equal('Virtual Circuit OCID', data.response.authKeyLabel);
                assert.equal('Example company Virtual Circuit', data.response.connectionNameLabel);
                assert.equal(true, data.response.equinixManagedPortAndVlan);
                assert.equal('Example-company-CrossConnect-01', data.response.integrationId);
                assert.equal(true, data.response.apiAvailable);
                assert.equal(true, data.response.allowOverSubscription);
                assert.equal('5x', data.response.overSubscription);
                assert.equal(true, data.response.vlanSameAsPrimary);
                assert.equal('CTAGED', data.response.tagType);
                assert.equal(false, data.response.enableAutoGenerateServiceKey);
                assert.equal('Seller-Side C-Tag', data.response.ctagLabel);
                assert.equal(true, Array.isArray(data.response.namedTags));
                assert.equal(50, data.response.alertPercentage);
                assert.equal(true, Array.isArray(data.response.onProfileApprovalRejectNotification));
                assert.equal(true, Array.isArray(data.response.onBandwidthThresholdNotification));
                assert.equal(true, Array.isArray(data.response.onVcApprovalRejectionNotification));
                assert.equal('object', typeof data.response.ports);
                assert.equal(false, data.response.allowCustomSpeed);
                assert.equal(true, data.response.speedFromAPI);
                assert.equal('HYBRID', data.response.connectionAccessibility);
                assert.equal(true, Array.isArray(data.response.speedBands));
                assert.equal('<p>12</p>', data.response.description);
                assert.equal('string', data.response.authorizationKey);
                assert.equal('APPROVED', data.response.state);
                assert.equal('2017-05-09T08:28:02Z', data.response.createdDate);
                assert.equal('sandboxuser@examplecompany.com', data.response.createdBy);
                assert.equal('2018-04-03T20:28:43.911Z', data.response.lastUpdatedDate);
                assert.equal('sandboxuser2@examplecompany.com2', data.response.lastUpdatedBy);
                assert.equal(12345, data.response.custOrgId);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('sandboxuser', data.response.createdByFullName);
                assert.equal('sandboxuser', data.response.lastUpdatedByFullName);
                assert.equal('sandboxuser2@examplecompany.com', data.response.createdByEmail);
                assert.equal('sandboxuser2@examplecompany.com', data.response.updatedByEmail);
                assert.equal('Example provider', data.response.globalOrganization);
                assert.equal('oracle Public Cloud Group ', data.response.organizationName);
                assert.equal(true, data.response.private);
                assert.equal(true, data.response.allowHighAvailability);
                assert.equal(false, data.response.allowAuthorizationKeyReUse);
                assert.equal(true, data.response.allowSecondaryLocation);
                assert.equal('object', typeof data.response.features);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceProfiles', 'getProfileByIdOrNameUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const connectionsPerformUserActionUsingPATCHBodyParam = {
      primaryPortUUID: '62637774-xxxx-d580-xxxx-30ac1885134a',
      primaryVlanSTag: '89',
      primaryVlanCTag: '809',
      secondaryPortUUID: '62637774-xxxx-d540-xxxx-xxxxxxxxxxxxx',
      secondaryVlanSTag: '98',
      secondaryVlanCTag: '980',
      rejectComment: 'rejected'
    };
    describe('#getSellerConnectionsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSellerConnectionsUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(true, data.response.isFirstPage);
                assert.equal(false, data.response.isLastPage);
                assert.equal(1, data.response.pageNumber);
                assert.equal(210, data.response.totalCount);
                assert.equal(20, data.response.pageSize);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'getSellerConnectionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmV1SmartviewAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmV1SmartviewAlarms('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.totalCount);
                assert.equal(true, Array.isArray(data.response.alarms));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'getAlarmV1SmartviewAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlerts(null, null, null, null, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.payLoad));
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertActivityLog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertActivityLog(null, null, null, null, null, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.payLoad));
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertLog', 'getAlertActivityLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAsset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAsset('fakedata', 'fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'getAsset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssetDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssetDetails('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'getAssetDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCIMAssetsPostSmartviewV1AssetDetailsBodyParam = {
      accountNo: 'string',
      ibx: 'string',
      classification: 'string',
      assetIds: [
        'string'
      ]
    };
    describe('#postSmartviewV1AssetDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSmartviewV1AssetDetails(dCIMAssetsPostSmartviewV1AssetDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'postSmartviewV1AssetDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAffectedAsset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAffectedAsset('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'getAffectedAsset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAsset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAsset('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'searchAsset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentTagPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentTagPoint('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.payLoad));
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'getCurrentTagPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCIMAssetsPostSmartviewV1AssetTagpointCurrentBodyParam = {
      accountNo: 'string',
      tagIds: [
        'string'
      ],
      ibx: 'string'
    };
    describe('#postSmartviewV1AssetTagpointCurrent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSmartviewV1AssetTagpointCurrent(dCIMAssetsPostSmartviewV1AssetTagpointCurrentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.payLoad));
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'postSmartviewV1AssetTagpointCurrent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagpointTrending - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTagpointTrending('fakedata', 'fakedata', 'fakedata', 'fakedata', 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMAssets', 'getTagpointTrending', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnvironmentV1Current - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEnvironmentV1Current('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMEnvironment', 'getEnvironmentV1Current', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnvironmentV1ListCurrent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEnvironmentV1ListCurrent('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMEnvironment', 'getEnvironmentV1ListCurrent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEnvironmentV1Trending - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEnvironmentV1Trending('fakedata', 555, 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMEnvironment', 'getEnvironmentV1Trending', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocationHierarchy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLocationHierarchy('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMHierarchyAPIs', 'getLocationHierarchy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPowerHierarchy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPowerHierarchy('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMHierarchyAPIs', 'getPowerHierarchy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPowerV1Current - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPowerV1Current('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMPower', 'getPowerV1Current', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCIMPowerPostPowerV1CurrentBodyParam = {
      accountNo: '1234',
      ibx: '0.147',
      levelType: '0.147'
    };
    describe('#postPowerV1Current - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPowerV1Current(dCIMPowerPostPowerV1CurrentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMPower', 'postPowerV1Current', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPowerV1Trending - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPowerV1Trending('fakedata', 555, 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.payLoad);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCIMPower', 'getPowerV1Trending', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSensorReadings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSensorReadings('fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.pagination);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Environmental', 'getSensorReadings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleSensorReadings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSingleSensorReadings('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SV2.Environmental.Colo4-ZoneHumidity1', data.response.sensorId);
                assert.equal('CH2:1:06:ColoArea:1', data.response.zoneId);
                assert.equal('SV2', data.response.ibx);
                assert.equal('object', typeof data.response.humidity);
                assert.equal('object', typeof data.response.temperature);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Environmental', 'getSingleSensorReadings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const assetsSearchAssetsBodyParam = {
      filter: {
        ibxs: [
          'string'
        ],
        cages: [
          'string'
        ],
        productTypes: [
          'CABINET'
        ],
        dateRange: {
          fromDate: 'string',
          toDate: 'string'
        }
      }
    };
    describe('#searchAssets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAssets(null, null, null, null, null, null, assetsSearchAssetsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Assets', 'searchAssets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssetById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Assets', 'getAssetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAttachment('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('56d10de6-f2c0-4edd-ba29-b70736aa2093', data.response.attachmentId);
                assert.equal('Example_Invoice.xlsx', data.response.attachmentName);
                assert.equal('xlsx', data.response.attachmentType);
                assert.equal(259387, data.response.attachmentSize);
                assert.equal('2017-01-31T09:03:08.340Z', data.response.createdDate);
                assert.equal('john.doe@example.com', data.response.createdBy);
                assert.equal('2017-04-06T17:10:44.807Z', data.response.lastUpdatedDate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'createAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAttachment('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'deleteAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrossConnectProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCrossConnectProviders('fakedata', 'fakedata', 'fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'getCrossConnectProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrossConnectLocations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCrossConnectLocations(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'getCrossConnectLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrossConnectTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCrossConnectTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.crossconnects));
                assert.equal(true, Array.isArray(data.response.interconnectionservices));
                assert.equal(true, Array.isArray(data.response.networkports));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'getCrossConnectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrossconnectConnectionServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCrossconnectConnectionServices(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'getCrossconnectConnectionServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCrossConnectPatchPanel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCrossConnectPatchPanel(null, null, null, null, null, null, null, null, null, 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SV1', data.response.ibx);
                assert.equal('2315', data.response.cage);
                assert.equal('Mixed', data.response.dedicatedMediaType);
                assert.equal('No', data.response.prewired);
                assert.equal('12', data.response.maxPorts);
                assert.equal('Equinix provided', data.response.ppType);
                assert.equal('Z424', data.response.customerPanelRefNumber);
                assert.equal('Back', data.response.rackLoc);
                assert.equal('abc', data.response.installLoc);
                assert.equal(true, data.response.installReq);
                assert.equal(true, Array.isArray(data.response.mediaTypes));
                assert.equal(true, Array.isArray(data.response.availablePorts));
                assert.equal(true, Array.isArray(data.response.usedPorts));
                assert.equal(true, Array.isArray(data.response.invalidPorts));
                assert.equal(true, Array.isArray(data.response.reservedPorts));
                assert.equal('PP:0103:1083488', data.response.ppNumber);
                assert.equal('0103', data.response.cabinetNumber);
                assert.equal(true, Array.isArray(data.response.ppNumberList));
                assert.equal(true, Array.isArray(data.response.ppDetailList));
                assert.equal(true, Array.isArray(data.response.connectionServices));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'getCrossConnectPatchPanel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalAvailabilityPostCrossConnectStandardBodyParam = {
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#postCrossConnectStandard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCrossConnectStandard('fakedata', generalAvailabilityPostCrossConnectStandardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.OrderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'postCrossConnectStandard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalAvailabilityPostCrossConnectsStandardBodyParam = {
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: [
        {}
      ]
    };
    describe('#postCrossConnectsStandard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCrossConnectsStandard('fakedata', generalAvailabilityPostCrossConnectsStandardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.OrderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralAvailability', 'postCrossConnectsStandard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBillingAccounts(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'getBillingAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadBillingDocument - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadBillingDocument('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'downloadBillingDocument', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingAccountByNumber - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBillingAccountByNumber('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('124353', data.response.accountNumber);
                assert.equal('object', typeof data.response.billingContact);
                assert.equal('MONTHLY', data.response.billingFrequency);
                assert.equal('English', data.response.invoiceLanguage);
                assert.equal('PDF', data.response.invoiceFormat);
                assert.equal(true, Array.isArray(data.response.invoices));
                assert.equal('GBP', data.response.currencyCode);
                assert.equal(true, Array.isArray(data.response.payments));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'getBillingAccountByNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ibxNotificationSearchIbxNotificationsBodyParam = {
      filter: {
        ibxs: [
          'SG2',
          'SV1'
        ],
        types: [
          'IBX_INCIDENT',
          'IBX_ADVISORY'
        ],
        statuses: [
          'RESCHEDULED',
          'NEW'
        ],
        dateRange: {
          fromDate: '2018-01-28T03:46:36.720Z',
          toDate: '2018-03-28T03:46:36.720Z'
        }
      }
    };
    describe('#searchIbxNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchIbxNotifications(null, null, null, ibxNotificationSearchIbxNotificationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IbxNotification', 'searchIbxNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIbxNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIbxNotification('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('5-122719992195', data.response.id);
                assert.equal('IBX_ADVISORY', data.response.type);
                assert.equal('2018-01-28T03:46:36.720Z', data.response.startTimestamp);
                assert.equal('2018-01-29T03:46:36.720Z', data.response.endTimestamp);
                assert.equal(true, Array.isArray(data.response.ibxs));
                assert.equal('RESOLVED', data.response.status);
                assert.equal('Fire Alarm - IBX was Evacuated', data.response.summary);
                assert.equal(true, Array.isArray(data.response.emails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IbxNotification', 'getIbxNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNotificationSearchNetworkNotificationsBodyParam = {
      filter: {
        ibxs: [
          'CH2',
          'SG1'
        ],
        types: [
          'NETWORK_INCIDENT'
        ],
        productTypes: [
          'CLOUD_EXCHANGE',
          'METRO_CONNECT'
        ],
        statuses: [
          'RESCHEDULED',
          'NEW'
        ],
        dateRange: {
          fromDate: '2018-01-28T03:46:36.720Z',
          toDate: '2018-03-28T03:46:36.720Z'
        }
      }
    };
    describe('#searchNetworkNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchNetworkNotifications(null, null, null, networkNotificationSearchNetworkNotificationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkNotification', 'searchNetworkNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkNotification('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('5-122662504647', data.response.id);
                assert.equal('NETWORK_MAINTENANCE', data.response.type);
                assert.equal('2018-01-28T03:46:36.720Z', data.response.startTimestamp);
                assert.equal('2018-01-29T03:46:36.720Z', data.response.endTimestamp);
                assert.equal(true, Array.isArray(data.response.ibxs));
                assert.equal('RESOLVED', data.response.status);
                assert.equal(true, Array.isArray(data.response.productTypes));
                assert.equal('line card replacement of dis01.wa1', data.response.summary);
                assert.equal(true, Array.isArray(data.response.emails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkNotification', 'getNetworkNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRetrieveOrdersLocations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRetrieveOrdersLocations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RetrieveOrders', 'getRetrieveOrdersLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const retrieveOrdersPostOrdersHistoryBodyParam = {
      filters: {
        orderStatus: [
          'PENDING_QA',
          'IN_PROGRESS',
          'PARTIAL_SUBMISSION'
        ],
        productTypes: [
          'SMART_HANDS'
        ],
        ibxs: [
          'SV1'
        ],
        fromDate: '05/01/2018',
        toDate: '05/02/2018'
      },
      source: [
        'CONTACT_LAST_NAME'
      ],
      q: '1-123456789',
      sorts: [
        {
          name: 'ORDERING_CONTACT',
          direction: 'DESC'
        }
      ],
      page: {
        number: 5,
        size: 25
      }
    };
    describe('#postOrdersHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOrdersHistory(retrieveOrdersPostOrdersHistoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal('object', typeof data.response.page);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RetrieveOrders', 'postOrdersHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportCenterScheduleReportBodyParam = {
      name: 'string',
      parameters: [
        {
          name: 'user_key',
          value: '143534,908373',
          type: 'ARRAY',
          required: true
        }
      ],
      scheduleType: 'DAILY',
      period: '30_DAYS'
    };
    describe('#scheduleReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.scheduleReport(reportCenterScheduleReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('fec45434-dd6a-4fe9-8fd4-06f219e5dff6', data.response.scheduledId);
                assert.equal('eiusmod qui nisi', data.response.reportName);
                assert.equal('QUARTERLY', data.response.scheduleType);
                assert.equal('14_DAYS', data.response.period);
                assert.equal('reprehenderit', data.response.createdBy);
                assert.equal('4804-01-02T06:54:41.561Z', data.response.createdDate);
                assert.equal('4533-03-23T21:44:07.660Z', data.response.lastAttemptedDate);
                assert.equal(75750444, data.response.customerOrganizationId);
                assert.equal(36659, data.response.forOrg);
                assert.equal(59730, data.response.forUser);
                assert.equal('john.doe', data.response.lastModifiedBy);
                assert.equal('2017-07-02T21:59:15.045Z', data.response.lastModifiedDate);
                assert.equal(1, data.response.numberOfFailedAttempts);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, Array.isArray(data.response.parameters));
                assert.equal(true, Array.isArray(data.response.reports));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'scheduleReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduledReports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduledReports(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'getScheduledReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScheduledReports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteScheduledReports([], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'deleteScheduledReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReports - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadReports([], (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'downloadReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduledReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduledReport('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('7a48fc3e-5263-4d75-a6ea-a1c576318ec5', data.response.scheduledId);
                assert.equal('nisi', data.response.reportName);
                assert.equal('MONTHLY', data.response.scheduleType);
                assert.equal('7_DAYS', data.response.period);
                assert.equal('ut ullamco in qui', data.response.createdBy);
                assert.equal('3549-12-27T03:27:05.019Z', data.response.createdDate);
                assert.equal('3732-11-15T13:24:17.660Z', data.response.lastAttemptedDate);
                assert.equal(86501527, data.response.customerOrganizationId);
                assert.equal(80446188, data.response.forOrg);
                assert.equal(12354, data.response.forUser);
                assert.equal('john.doe', data.response.lastModifiedBy);
                assert.equal('2017-07-02T21:59:15.045Z', data.response.lastModifiedDate);
                assert.equal(3, data.response.numberOfFailedAttempts);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(true, Array.isArray(data.response.parameters));
                assert.equal(true, Array.isArray(data.response.reports));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'getScheduledReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportCenterUpdateScheduledReportBodyParam = {
      name: 'string',
      parameters: [
        {
          name: 'user_key',
          value: '143534,908373',
          type: 'ARRAY',
          required: true
        }
      ],
      scheduleType: 'ONE_TIME',
      period: '90_DAYS'
    };
    describe('#updateScheduledReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateScheduledReport('fakedata', reportCenterUpdateScheduledReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'updateScheduledReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportDefinitions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportDefinitions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'getReportDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReports([], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'deleteReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReports(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'getReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateReport('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('eab1d606-2315-476f-848a-a622a47f24ff', data.response.reportId);
                assert.equal('b48696e4-6fdb-49ed-868e-040df6596245', data.response.scheduledId);
                assert.equal('4498-05-31T11:48:13.373Z', data.response.startTime);
                assert.equal('4472-01-16T11:56:02.238Z', data.response.endTime);
                assert.equal('elit consequat occaecat', data.response.createdBy);
                assert.equal('2251-10-21T02:07:18.598Z', data.response.createdDate);
                assert.equal('occaecat ullamco tempor c', data.response.lastAccessedBy);
                assert.equal('2594-04-16T16:02:41.030Z', data.response.lastAccessedDate);
                assert.equal('IN_PROGRESS', data.response.status);
                assert.equal('eu labore', data.response.location);
                assert.equal(1, data.response.numberOfDownloads);
                assert.equal(1, data.response.numberOfAttempts);
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'generateReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportDefinition('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('consectetur in amet ullamco occaecat', data.response.name);
                assert.equal(true, Array.isArray(data.response.parameters));
                assert.equal('ALLOW', data.response.control);
                assert.equal(true, Array.isArray(data.response.categories));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'getReportDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadReport('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'downloadReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('894d2b14-30b2-413b-8370-110998b9923d', data.response.reportId);
                assert.equal('102eb0a5-0d4b-46ba-8865-773728cfffbb', data.response.scheduledId);
                assert.equal('ad dolor', data.response.createdBy);
                assert.equal('2909-07-21T00:14:20.700Z', data.response.createdDate);
                assert.equal('eu reprehenderit', data.response.lastAccessedBy);
                assert.equal('3111-04-13T18:17:23.826Z', data.response.lastAccessedDate);
                assert.equal('3698-08-25T04:24:44.011Z', data.response.startTime);
                assert.equal('1994-03-01T04:43:58.554Z', data.response.endTime);
                assert.equal('ERROR', data.response.status);
                assert.equal('cupidatat qui non', data.response.errorMessage);
                assert.equal('Excepteur fugiat consequat Ut eu', data.response.location);
                assert.equal(92915061, data.response.numberOfDownloads);
                assert.equal(1, data.response.numberOfAttempts);
                assert.equal(true, Array.isArray(data.response.parameters));
                assert.equal('object', typeof data.response.publisherInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportCenter', 'getReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentOutboundShipmentSubmitBodyParam = {
      contacts: [
        {}
      ],
      ibxLocation: {
        ibx: 'PA2',
        cages: [
          {
            cage: 'PA2:02:002MC1'
          }
        ]
      },
      serviceDetails: {}
    };
    describe('#outboundShipmentSubmit - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.outboundShipmentSubmit(shipmentOutboundShipmentSubmitBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1-128726682521', data.response.OrderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'outboundShipmentSubmit', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLocation(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.locations));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'getLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentShipmentInboundBodyParam = {
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      ibxLocation: {
        ibx: 'PA2',
        cages: [
          {
            cage: 'PA2:02:002MC1'
          }
        ]
      },
      serviceDetails: {
        estimatedDateTime: '2018-10-15T05:00:00.420-08:00',
        shipmentDetails: {
          noOfBoxes: 10,
          isOverSized: true,
          description: 'hello',
          trackingNumber: [
            '1235467869',
            '1',
            '4',
            '5'
          ],
          inboundType: 'CARRIER',
          carrierName: 'OTHER',
          otherCarrierName: 'test'
        },
        deliverToCage: true,
        inboundRequestDescription: 'hello'
      }
    };
    describe('#shipmentInbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.shipmentInbound(shipmentShipmentInboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1-128726682522', data.response.OrderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'shipmentInbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentShipmentPendingBodyParam = {
      shipmentDetails: [
        {}
      ],
      contacts: [
        {}
      ]
    };
    describe('#shipmentPending - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.shipmentPending(shipmentShipmentPendingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1-128726682522', data.response.OrderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'shipmentPending', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandOrder(smarthandsHandleSmartHandOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandShipmentUnpackOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandShipmentUnpackOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandShipmentUnpackOrder(smarthandsHandleSmartHandShipmentUnpackOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandShipmentUnpackOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandMoveJumperCableOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandMoveJumperCableOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandMoveJumperCableOrder(smarthandsHandleSmartHandMoveJumperCableOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandMoveJumperCableOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandCageEscortOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandCageEscortOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandCageEscortOrder(smarthandsHandleSmartHandCageEscortOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandCageEscortOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandLocatePackageOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandLocatePackageOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandLocatePackageOrder(smarthandsHandleSmartHandLocatePackageOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandLocatePackageOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandPicturesDocumentOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandPicturesDocumentOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandPicturesDocumentOrder(smarthandsHandleSmartHandPicturesDocumentOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandPicturesDocumentOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandPatchCableInstallOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandPatchCableInstallOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandPatchCableInstallOrder(smarthandsHandleSmartHandPatchCableInstallOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandPatchCableInstallOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandPatchCableRemovalOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandPatchCableRemovalOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandPatchCableRemovalOrder(smarthandsHandleSmartHandPatchCableRemovalOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandPatchCableRemovalOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandCageCleanupOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'TECHNICAL',
          name: 'Jane Smith',
          email: 'jane_smith@test.com',
          workPhoneCountryCode: '+1',
          workPhone: '408123456',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhoneCountryCode: '+1',
          mobilePhone: '408123456',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandCageCleanupOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandCageCleanupOrder(smarthandsHandleSmartHandCageCleanupOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandCageCleanupOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandCableRequestOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandCableRequestOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandCableRequestOrder(smarthandsHandleSmartHandCableRequestOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandCableRequestOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandRunJumperCableOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandRunJumperCableOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandRunJumperCableOrder(smarthandsHandleSmartHandRunJumperCableOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandRunJumperCableOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const smarthandsHandleSmartHandOthersOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          email: 'jane_smith@test.com',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      schedule: {},
      serviceDetails: {}
    };
    describe('#handleSmartHandOthersOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleSmartHandOthersOrder(smarthandsHandleSmartHandOthersOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-equinix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'handleSmartHandOthersOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#smartHandTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.smartHandTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.smarthands));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Smarthands', 'smartHandTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTroubleTicketType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTroubleTicketType(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.troubleticketTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Troubleticket', 'getTroubleTicketType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const troubleticketPlaceTroubleTicketOrderBodyParam = {
      ibxLocation: {},
      contacts: [
        {}
      ],
      serviceDetails: {}
    };
    describe('#placeTroubleTicketOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.placeTroubleTicketOrder(troubleticketPlaceTroubleTicketOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1-128726682521', data.response.OrderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Troubleticket', 'placeTroubleTicketOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchUser(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.page);
                assert.equal('object', typeof data.response._links);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'searchUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workvisitWorkVisitOrderBodyParam = {
      contacts: [
        {
          contactType: 'ORDERING',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'NOTIFICATION',
          userName: 'jondoe@test.com'
        },
        {
          contactType: 'TECHNICAL',
          name: 'John Doe',
          workPhone: '1111111',
          workPhonePrefToCall: 'ANYTIME',
          mobilePhone: '1111111',
          mobilePhonePrefToCall: 'ANYTIME'
        }
      ],
      ibxLocation: {},
      serviceDetails: {}
    };
    describe('#workVisitOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.workVisitOrder(workvisitWorkVisitOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.successes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workvisit', 'workVisitOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
