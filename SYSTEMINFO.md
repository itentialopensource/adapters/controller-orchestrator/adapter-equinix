# Equinix

Vendor: Equinix
Homepage: https://www.equinix.com/

Product: Network Edge
Product Page: https://developer.equinix.com/docs?page=/dev-docs/ne/overview

## Introduction
We classify Equinix into the Data Center and Network Services domains as Equinix Network Edge provides essential interconnection services that support digital business and cloud infrastructure, facilitating secure and scalable connectivity solutions​. 

## Why Integrate
The Equinix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Equinix Network Edge.

With this adapter you have the ability to perform operations with Equinix on items such as:

- VPN
- BGP
- Layer 2
- Devices

## Additional Product Documentation
The [API documents for Equinix](https://developer.equinix.com/docs?page=/dev-docs/ne/overview)

