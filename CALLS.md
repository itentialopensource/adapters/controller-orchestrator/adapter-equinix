## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Equinix. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Equinix.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Equinix. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualDevicesUsingGET(deviceTypeCode, category, offset, limit, callback)</td>
    <td style="padding:15px">Get Device Types</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/deviceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetrosUsingGET(region, offset, limit, callback)</td>
    <td style="padding:15px">Get Available Metros</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/metros?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsWithStatusUsingGET(metro, accountUcmId, callback)</td>
    <td style="padding:15px">Get Accounts {metro}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAgreementStatusUsingGET(accountNumber, callback)</td>
    <td style="padding:15px">Get Agreement Status.</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/agreements/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendAgreementUsingPOST1(agreementAcceptRequest, callback)</td>
    <td style="padding:15px">Create an agreement</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/agreements/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorTermsUsingGET(vendorPackage, licenseType, callback)</td>
    <td style="padding:15px">Get Vendor Terms</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/agreements/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrderTermsUsingGET(callback)</td>
    <td style="padding:15px">Get Order Terms</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/agreements/orders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievePriceUsingGET(accountNumber, metro, vendorPackage, licenseType, softwarePackage, throughput, throughputUnit, termLength, additionalBandwidth, virtualDeviceUuid, deviceManagementType, core, secondaryAccountNumber, secondaryMetro, secondaryAdditionalBandwidth, accountUcmId, orderingContact, callback)</td>
    <td style="padding:15px">Get the Price</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/prices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrderSummaryUsingGET(accountNumber, metro, vendorPackage, licenseType, softwarePackage, throughput, throughputUnit, termLength, additionalBandwidth, virtualDeviceUuid, deviceManagementType, core, secondaryAccountNumber, secondaryMetro, secondaryAdditionalBandwidth, accountUcmId, orderingContact, callback)</td>
    <td style="padding:15px">Get Order Summary</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/orderSummaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendDnsLookupPOST1(dnsLookupRequest, callback)</td>
    <td style="padding:15px">Post DNS Lookup</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/dnsLookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicKeysUsingGET(accountUcmId, callback)</td>
    <td style="padding:15px">Get Public Keys</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/publicKeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPublicKeyUsingPOST(publicKeyRequest, callback)</td>
    <td style="padding:15px">Create Public Key</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/publicKeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadLicenseUsingPOST(file, metroCode, deviceTypeCode, licenseType, callback)</td>
    <td style="padding:15px">Post License File</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/licenseFiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadLicenseForDeviceUsingPOST(uuid, file, callback)</td>
    <td style="padding:15px">Post License {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/licenseFiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLicenseUsingPOST(uuid, request, callback)</td>
    <td style="padding:15px">Update License Token/ID/Code</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}/licenseTokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVirtualDeviceUsingPOST(virtualDevice, draft, draftUuid, callback)</td>
    <td style="padding:15px">Create Virtual Device</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualDevicesUsingGET1(offset, limit, metroCode, status, showOnlySubCustomerDevices, accountUcmId, callback)</td>
    <td style="padding:15px">Get Virtual Devices</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualDeviceUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get Virtual Device {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVirtualDeviceUsingPATCH1(uuid, virtualDeviceUpdateRequestDto, callback)</td>
    <td style="padding:15px">Update Virtual Device</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVRouterUsingDELETE(uuid, deletionInfo, deleteRedundantDevice, callback)</td>
    <td style="padding:15px">Delete Virtual Devices</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVirtualDeviceUsingPUT(draft, virtualDevice, uuid, callback)</td>
    <td style="padding:15px">Update Device Draft</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdditionalBandwidth(uuid, request, callback)</td>
    <td style="padding:15px">Update Additional Bandwidth</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}/additionalBandwidths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualDeviceInterfacesUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get Device Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingDeviceUsingGET(uuid, callback)</td>
    <td style="padding:15px">Ping Virtual Device</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/devices/{pathv1}/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceACLTemplateUsingPost(aclTemplateRequest, accountUcmId, callback)</td>
    <td style="padding:15px">Create ACL Template</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/aclTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceACLTemplateUsingGET1(offset, limit, accountUcmId, callback)</td>
    <td style="padding:15px">Get ACL Templates</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/aclTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplatebyUuid(uuid, accountUcmId, callback)</td>
    <td style="padding:15px">Get ACL Template {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/aclTemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeviceACLUsingDELETE(uuid, accountUcmId, callback)</td>
    <td style="padding:15px">Delete ACL template</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/aclTemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceACLTemplateUsingPUT(uuid, accountUcmId, aclTemplateRequest, callback)</td>
    <td style="padding:15px">Update ACL Template</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/aclTemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLinkGroupUsingPOST(deviceLinkGroup, callback)</td>
    <td style="padding:15px">Create Device Link</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkGroupsUsingGET1(metro, virtualDeviceUuid, accountUcmId, pageNumber, size, callback)</td>
    <td style="padding:15px">Get Device Links.</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkGroupByUUIDUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get Device Link {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLinkGroupUsingPATCH(uuid, deviceLinkGroup, callback)</td>
    <td style="padding:15px">Update Device Link</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLinkGroupUsingDELETE(uuid, callback)</td>
    <td style="padding:15px">Delete Device Link</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfilesByMetroUsingGET(metroCode, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">Get L2 Service Profiles</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/serviceprofiles/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileByIdUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get L2 Service Profile {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/serviceprofiles/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateAuthorizationKeyUsingGET(authorizationKey, metroCode, profileId, region, callback)</td>
    <td style="padding:15px">Get L2 Validate Authorization Key</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/connections/validateAuthorizationKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnectionUsingPOST(request, callback)</td>
    <td style="padding:15px">Create L2 Connection</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performUserActionUsingPATCH(action, uuid, request, callback)</td>
    <td style="padding:15px">Update Connection {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectionByUuidUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get L2 Connection</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectionUsingDELETE(uuid, callback)</td>
    <td style="padding:15px">Delete Connection {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/l2/connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBgpConfigurationUsingPOST(request, callback)</td>
    <td style="padding:15px">Create BGP Peering</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBgpConfigurationsUsingGET(virtualDeviceUuid, connectionUuid, status, accountUcmId, offset, limit, callback)</td>
    <td style="padding:15px">Get BGP Peering</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBgpConfigurationUsingPUT(uuid, request, callback)</td>
    <td style="padding:15px">Update BGP Peering</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/bgp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBgpConfigurationUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get BGP Peering {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/bgp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpnUsingPOST(request, callback)</td>
    <td style="padding:15px">Create VPN Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnsUsingGET(statusList = 'PROVISIONED', virtualDeviceUuid, offset, limit, callback)</td>
    <td style="padding:15px">Get VPN Configurations</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnByUuidUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get VPN Configuration {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVpnConfigurationUsingPut(uuid, request, callback)</td>
    <td style="padding:15px">Update VPN Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeVpnConfigurationUsingDELETE(uuid, callback)</td>
    <td style="padding:15px">Delete VPN Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshUsersUsingGET(username, virtualDeviceUuid, verbose, accountUcmId, offset, limit, callback)</td>
    <td style="padding:15px">Get SSH Users</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSshUserUsingPOST(request, callback)</td>
    <td style="padding:15px">Create SSH User</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isSshUserAvailableForCreationUsingGET(username, callback)</td>
    <td style="padding:15px">SSH User Availability</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers/availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshUserUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get SSH User {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSshUserUsingPUT(uuid, request, callback)</td>
    <td style="padding:15px">Update SSH User Password</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dissociateDeviceUsingDELETE(sshUserUuid, deviceUuid, callback)</td>
    <td style="padding:15px">Delete SSH User</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateDeviceUsingPOST(sshUserUuid, deviceUuid, callback)</td>
    <td style="padding:15px">Update SSH User {uuid}</td>
    <td style="padding:15px">{base_path}/{version}/ne/v1/sshUsers/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerVersion(callback)</td>
    <td style="padding:15px">Get SmartKey version information</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkHealth(callback)</td>
    <td style="padding:15px">Check whether the server is handling requests</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorize(callback)</td>
    <td style="padding:15px">Create a session for a user or an app</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reAuth(callback)</td>
    <td style="padding:15px">Create a new session for a user or an app using an existing session bearer token.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/reauth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refresh(callback)</td>
    <td style="padding:15px">Refreshes existing user or app session.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminate(callback)</td>
    <td style="padding:15px">Terminate a session</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">selectAccount(body, callback)</td>
    <td style="padding:15px">Select a user's account to work on</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/select_account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlock2F(callback)</td>
    <td style="padding:15px">Unlock two factor configuration</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/config_2fa/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityObjects(name, groupId, creator, sort, compliantWithPolicies, start, limit, offset, callback)</td>
    <td style="padding:15px">Get all security objects</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateSecurityObject(body, callback)</td>
    <td style="padding:15px">Generate a new security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importSecurityObject(body, callback)</td>
    <td style="padding:15px">Import a security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityObject(keyId, callback)</td>
    <td style="padding:15px">Get a specific security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityObject(keyId, callback)</td>
    <td style="padding:15px">Delete a security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityObject(keyId, body, callback)</td>
    <td style="padding:15px">Update a security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">persistSecurityObject(body, callback)</td>
    <td style="padding:15px">Persist a transient key.</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/persist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePrivateKey(keyId, callback)</td>
    <td style="padding:15px">Remove / Destroy private half of the asymmetric key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/private?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rotateSecurityObject(body, callback)</td>
    <td style="padding:15px">Rotate a key.</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/rekey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityObjectValueEx(body, callback)</td>
    <td style="padding:15px">Retrieve the value of an exportable security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityObjectDigest(body, callback)</td>
    <td style="padding:15px">Retrieve the digest (hash) of the value of an exportable security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/digest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityObjectValue(keyId, callback)</td>
    <td style="padding:15px">Retrieve the value of an exportable security object</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateSecurityObject(keyId, callback)</td>
    <td style="padding:15px">Transitions immediately a security object to Active state</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeSecurityObject(keyId, body, callback)</td>
    <td style="padding:15px">Transitions immediately a security object to Deactivated or Compromised state</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deriveKey(keyId, body, callback)</td>
    <td style="padding:15px">Derive a key from another key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/derive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deriveKeyEx(body, callback)</td>
    <td style="padding:15px">Derive a key from another key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/derive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agreeKey(body, callback)</td>
    <td style="padding:15px">Agree on a key from two other keys</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/agree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encrypt(keyId, body, callback)</td>
    <td style="padding:15px">Encrypt data</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/encrypt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptEx(body, callback)</td>
    <td style="padding:15px">Encrypt data</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/encrypt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchEncrypt(body, callback)</td>
    <td style="padding:15px">Batch encrypt with one or more keys</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/batch/encrypt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptInit(keyId, body, callback)</td>
    <td style="padding:15px">Begin multi-part encryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/encrypt/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptInitEx(body, callback)</td>
    <td style="padding:15px">Begin multi-part encryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/encrypt/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptUpdate(keyId, body, callback)</td>
    <td style="padding:15px">Continue multi-part encryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/encrypt/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptUpdateEx(body, callback)</td>
    <td style="padding:15px">Continue multi-part encryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/encrypt/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptFinal(keyId, body, callback)</td>
    <td style="padding:15px">Conclude multi-part encryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/encrypt/final?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptFinalEx(body, callback)</td>
    <td style="padding:15px">Conclude multi-part encryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/encrypt/final?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decrypt(keyId, body, callback)</td>
    <td style="padding:15px">Decrypt data</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/decrypt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptEx(body, callback)</td>
    <td style="padding:15px">Decrypt data</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/decrypt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchDecrypt(body, callback)</td>
    <td style="padding:15px">Batch decrypt with one or more keys</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/batch/decrypt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptInit(keyId, body, callback)</td>
    <td style="padding:15px">Begin multi-part decryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/decrypt/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptInitEx(body, callback)</td>
    <td style="padding:15px">Begin multi-part decryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/decrypt/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptUpdate(keyId, body, callback)</td>
    <td style="padding:15px">Continue multi-part decryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/decrypt/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptUpdateEx(body, callback)</td>
    <td style="padding:15px">Continue multi-part decryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/decrypt/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptFinal(keyId, body, callback)</td>
    <td style="padding:15px">Conclude multi-part decryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/decrypt/final?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decryptFinalEx(body, callback)</td>
    <td style="padding:15px">Conclude multi-part decryption</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/decrypt/final?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sign(keyId, body, callback)</td>
    <td style="padding:15px">Sign with a private key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/sign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signEx(body, callback)</td>
    <td style="padding:15px">Sign with a private key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/sign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchSign(body, callback)</td>
    <td style="padding:15px">Batch sign with one or more private keys</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/batch/sign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verify(keyId, body, callback)</td>
    <td style="padding:15px">Verify a signature with a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyEx(body, callback)</td>
    <td style="padding:15px">Verify a signature with a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">batchVerify(body, callback)</td>
    <td style="padding:15px">Batch verify with one or more private keys</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/batch/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">computeDigest(body, callback)</td>
    <td style="padding:15px">Compute a message digest of data</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/digest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">computeMac(keyId, body, callback)</td>
    <td style="padding:15px">Compute MAC using a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/mac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">computeMacEx(body, callback)</td>
    <td style="padding:15px">Compute MAC using a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/mac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyMac(keyId, body, callback)</td>
    <td style="padding:15px">Verify MAC using a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/macverify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyMacEx(body, callback)</td>
    <td style="padding:15px">Verify MAC using a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/macverify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wrapKey(keyId, body, callback)</td>
    <td style="padding:15px">Wrap a security object with a key
</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/wrapkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wrapKeyEx(body, callback)</td>
    <td style="padding:15px">Wrap a security object with a key
</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/wrapkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unwrapKey(keyId, body, callback)</td>
    <td style="padding:15px">Unwrap a security object with a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/keys/{pathv1}/unwrapkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unwrapKeyEx(body, callback)</td>
    <td style="padding:15px">Unwrap a security object with a key</td>
    <td style="padding:15px">{base_path}/{version}/crypto/v1/unwrapkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApps(groupId, sort, start, limit, offset, callback)</td>
    <td style="padding:15px">Get all applications</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApp(body, callback)</td>
    <td style="padding:15px">Create a new application</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApp(appId, callback)</td>
    <td style="padding:15px">Get a specific application</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApp(appId, callback)</td>
    <td style="padding:15px">Delete application</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApp(appId, body, callback)</td>
    <td style="padding:15px">Update an application</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regenerateApiKey(appId, body, callback)</td>
    <td style="padding:15px">Regenerate API key</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps/{pathv1}/reset_secret?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredential(appId, callback)</td>
    <td style="padding:15px">Get a specific application's credential</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/apps/{pathv1}/credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(callback)</td>
    <td style="padding:15px">Get all groups</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">Create new group</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroup(groupId, callback)</td>
    <td style="padding:15px">Get a specific group</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(groupId, callback)</td>
    <td style="padding:15px">Delete group</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(groupId, body, callback)</td>
    <td style="padding:15px">Update group</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccounts(callback)</td>
    <td style="padding:15px">Get all accounts</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccount(body, callback)</td>
    <td style="padding:15px">Create a new account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccount(accountId, callback)</td>
    <td style="padding:15px">Get a specific account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccount(accountId, callback)</td>
    <td style="padding:15px">Delete account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccount(accountId, body, callback)</td>
    <td style="padding:15px">Update account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChildAccount(body, callback)</td>
    <td style="padding:15px">Create a new child account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts/child?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChildAccounts(accountId, callback)</td>
    <td style="padding:15px">Get all child accounts of the given account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/accounts/{pathv1}/child?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(groupId, sort, start, limit, offset, callback)</td>
    <td style="padding:15px">Get all users</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserAccount(callback)</td>
    <td style="padding:15px">Completely delete a user profile from system</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create a new user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inviteUser(body, callback)</td>
    <td style="padding:15px">Invite a user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/invite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(userId, callback)</td>
    <td style="padding:15px">Get a specific user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(userId, body, callback)</td>
    <td style="padding:15px">Update user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(body, callback)</td>
    <td style="padding:15px">Change user password</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/change_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserAccount(callback)</td>
    <td style="padding:15px">Get account information for the user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forgotPassword(body, callback)</td>
    <td style="padding:15px">Initiate password reset sequence for a user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/forgot_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPassword(userId, body, callback)</td>
    <td style="padding:15px">Reset a user's password</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}/reset_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processInvitations(body, callback)</td>
    <td style="padding:15px">Process a user's pending account invitations</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/process_invite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resendInvitation(userId, callback)</td>
    <td style="padding:15px">Resend invite to the user to join a specific account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}/resend_invite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userId, callback)</td>
    <td style="padding:15px">Removed user's association with an account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validatePasswordResetToken(userId, body, callback)</td>
    <td style="padding:15px">Validates password reset token for the user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}/validate_token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmEmail(userId, body, callback)</td>
    <td style="padding:15px">Confirms user's email address</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}/confirm_email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resendConfirmEmail(userId, callback)</td>
    <td style="padding:15px">Resend email with link to confirm user's email address</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/{pathv1}/resend_confirm_email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChildAccountUser(body, callback)</td>
    <td style="padding:15px">Create a new user user in a child account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/child_account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChildAccountUser(userId, callback)</td>
    <td style="padding:15px">Get a specific child account user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/child_account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateChildAccountUser(userId, body, callback)</td>
    <td style="padding:15px">Update a child account user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/child_account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteChildAccountUser(userId, callback)</td>
    <td style="padding:15px">Completely delete a user in child account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/child_account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllChildAccountUsers(accountId, callback)</td>
    <td style="padding:15px">Get all users in a child account</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/child_account/{pathv1}/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogs(size, from, actionType = 'Apps', actorType = 'Administrative', actorId, objectId, severity = 'Info', rangeFrom, rangeTo, callback)</td>
    <td style="padding:15px">Get audit logs</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsStats(rangeFrom, rangeTo, topCount, numPoints, callback)</td>
    <td style="padding:15px">Get group aggregate transaction statistics</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/stats/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppsStats(rangeFrom, rangeTo, topCount, numPoints, callback)</td>
    <td style="padding:15px">Get app aggregate transaction statistics</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/stats/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppStats(appId, rangeFrom, rangeTo, numPoints, callback)</td>
    <td style="padding:15px">Get transaction statistics for a specific application</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/stats/{pathv1}/app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupStats(groupId, rangeFrom, rangeTo, numPoints, callback)</td>
    <td style="padding:15px">Get transaction statistics for a specific group</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/stats/{pathv1}/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityObjectStats(keyId, rangeFrom, rangeTo, numPoints, callback)</td>
    <td style="padding:15px">Get transaction statistics for a specific security object</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/stats/{pathv1}/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeU2F(callback)</td>
    <td style="padding:15px">Use a U2F key to complete authentication</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/auth/2fa/u2f?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeRecoveryCode(callback)</td>
    <td style="padding:15px">Use a backup recovery code to complete authentication</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/auth/2fa/recovery_code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateU2FChallenge(callback)</td>
    <td style="padding:15px">Generate a new challenge for registering a U2F devices</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/config_2fa/new_challenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lock2F(callback)</td>
    <td style="padding:15px">Lock two factor configuration</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/session/config_2fa/terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateRecoveryCodes(callback)</td>
    <td style="padding:15px">Generate backup recovery codes for the current user</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/users/generate_recovery_code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlugins(groupId, sort, start, limit, offset, callback)</td>
    <td style="padding:15px">Get all plugins</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPlugin(body, callback)</td>
    <td style="padding:15px">Create a new plugin</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlugin(pluginId, callback)</td>
    <td style="padding:15px">Get a specific plugin</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePlugin(pluginId, callback)</td>
    <td style="padding:15px">Delete plugin</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePlugin(pluginId, body, callback)</td>
    <td style="padding:15px">Update a plugin</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invokePlugin(pluginId, body, callback)</td>
    <td style="padding:15px">Invoke a plugin</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysV1PluginsInvokePluginId(pluginId, callback)</td>
    <td style="padding:15px">Invoke a plugin using GET.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/plugins/invoke/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApprovalRequests(requester, reviewer, subject, status = 'PENDING', callback)</td>
    <td style="padding:15px">Get all approval requests</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApprovalRequest(body, callback)</td>
    <td style="padding:15px">Create approval request</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApprovalRequest(requestId, callback)</td>
    <td style="padding:15px">Get an approval request.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApprovalRequest(requestId, callback)</td>
    <td style="padding:15px">Delete an approval request.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approve(requestId, callback)</td>
    <td style="padding:15px">Approve a request.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests/{pathv1}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deny(requestId, callback)</td>
    <td style="padding:15px">Deny a request.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests/{pathv1}/deny?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResult(requestId, callback)</td>
    <td style="padding:15px">Get the result for an approved or failed request.</td>
    <td style="padding:15px">{base_path}/{version}/sys/v1/approval_requests/{pathv1}/result?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortInfoUsingGET2(callback)</td>
    <td style="padding:15px">Fetch List of Virtual Ports.</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/port/userport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStatUsingGET(portUUID, startDate, endDate, callback)</td>
    <td style="padding:15px">Get port utilization stats</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/stats/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBuyerConnectionsUsingGET(authorizationKey, status = 'PENDING_APPROVAL', metroCode, buyerPortName, buyerPortUUID, searchType = 'AND', subAccount, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">Get all my connections</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/buyer/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2PricesUsingGET(destinationMetro, customSpeeds, portUUID, callback)</td>
    <td style="padding:15px">Get Layer 2 connection pricing</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/connections/prices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBuyerPreferenceUsingGET(callback)</td>
    <td style="padding:15px">Get bandwidth alert preference</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/buyerPreference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveBuyerPreferenceUsingPOST(request, callback)</td>
    <td style="padding:15px">Create bandwidth alert preference</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/buyerPreference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBuyerPreferenceUsingPUT(request, callback)</td>
    <td style="padding:15px">Update bandwidth alert preference</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/buyerPreference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBuyerPreferenceUsingDELETE(callback)</td>
    <td style="padding:15px">Delete bandwidth alert preference</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/buyerPreference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSellerProfileUsingGET(pageNumber, pageSize, state = 'PENDING_APPROVAL', callback)</td>
    <td style="padding:15px">Get all service profiles</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/serviceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceProfileUsingPOST(profileModel, callback)</td>
    <td style="padding:15px">Create Layer 2 Service profile</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/serviceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL2ServiceProfileUsingPUT(profileModel, callback)</td>
    <td style="padding:15px">Update Layer 2 Service profile</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/serviceprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateIntegrationIdUsingGET(integrationId, callback)</td>
    <td style="padding:15px">Validate integration id</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/serviceprofiles/validateIntegrationId/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProfileByProfileNameOrUUIDUsingDELETE(uuid, callback)</td>
    <td style="padding:15px">Delete service profile by uuid</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/serviceprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileByIdOrNameUsingGET(uuid, callback)</td>
    <td style="padding:15px">Get service profiles</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/serviceprofiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSellerConnectionsUsingGET(status = 'PROVISIONED', metroCode, portName, state, profileId, pageNumber, pageSize, callback)</td>
    <td style="padding:15px">Get all of my connections</td>
    <td style="padding:15px">{base_path}/{version}/ecx/v3/l2/seller/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmV1SmartviewAlarms(accountNo, limit, offset, groupByIBX, callback)</td>
    <td style="padding:15px">Find all the active alarms.</td>
    <td style="padding:15px">{base_path}/{version}/alarm/v1/smartview/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlerts(ibx, category, eventType, accountNo, pageNum, limit, orderBy, sortBy, callback)</td>
    <td style="padding:15px">obtain active SmartView alerts.</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/alerts/getAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertActivityLog(ibx, category, eventType, isAcknowledged, accountNo, pageNum, limit, orderBy, sortBy, callback)</td>
    <td style="padding:15px">get alert activity logs</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/alerts/getAlertActivityLog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAsset(accountNo, ibx, classification = 'Electrical', cages, callback)</td>
    <td style="padding:15px">get assets list information</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetDetails(accountNo, ibx, classification, assetId, callback)</td>
    <td style="padding:15px">get details for an asset.</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSmartviewV1AssetDetails(payload, callback)</td>
    <td style="padding:15px">get asset details</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAffectedAsset(accountNo, ibx, assetId, classification = 'Electrical', callback)</td>
    <td style="padding:15px">get affected customers assets hierarchy</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/tagpoint/affected-assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAsset(accountNo, ibx, searchString, callback)</td>
    <td style="padding:15px">Search for Assets matching identifiers</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentTagPoint(accountNo, ibx, tagid, callback)</td>
    <td style="padding:15px">obtain latest tag point data</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/tagpoint/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSmartviewV1AssetTagpointCurrent(payload, callback)</td>
    <td style="padding:15px">get current tag points data</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/tagpoint/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagpointTrending(accountNo, ibx, tagid, interval, fromDate, toDate, callback)</td>
    <td style="padding:15px">obtain trending tag point data</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/asset/tagpoint/trending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnvironmentV1Current(accountNo, ibx, levelType = 'IBX', levelValue, callback)</td>
    <td style="padding:15px">Get current environmental data</td>
    <td style="padding:15px">{base_path}/{version}/environment/v1/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnvironmentV1ListCurrent(accountNo, ibx, levelType = 'IBX', callback)</td>
    <td style="padding:15px">Fetch list of environmental data</td>
    <td style="padding:15px">{base_path}/{version}/environment/v1/listCurrent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnvironmentV1Trending(accountNo, ibx, dataPoint = 'temperature', levelType = 'IBX', levelValue, interval = 'reading', fromDate, toDate, callback)</td>
    <td style="padding:15px">Fetch trending environmental data</td>
    <td style="padding:15px">{base_path}/{version}/environment/v1/trending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationHierarchy(accountNo, ibx, callback)</td>
    <td style="padding:15px">Fetch the Location Hierarchy</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/hierarchy/location?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPowerHierarchy(accountNo, ibx, callback)</td>
    <td style="padding:15px">Fetch the Power Hierarchy</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v1/hierarchy/power?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPowerV1Current(accountNo, ibx, levelType = 'ibx', levelValue, callback)</td>
    <td style="padding:15px">Fetch current power consumption data</td>
    <td style="padding:15px">{base_path}/{version}/power/v1/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPowerV1Current(body, callback)</td>
    <td style="padding:15px">Fetch current power consumption data</td>
    <td style="padding:15px">{base_path}/{version}/power/v1/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPowerV1Trending(accountNo, ibx, levelType = 'ibx', levelValue, interval, fromDate, toDate, callback)</td>
    <td style="padding:15px">Fetch Trending Power Data.
</td>
    <td style="padding:15px">{base_path}/{version}/power/v1/trending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSensorReadings(ibx, type, zone, offset, limit, callback)</td>
    <td style="padding:15px">IBX sensors current readings</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v2/environmental/ibxs/{pathv1}/sensors/readings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleSensorReadings(ibx, sensorId, callback)</td>
    <td style="padding:15px">Single sensor current value(s)</td>
    <td style="padding:15px">{base_path}/{version}/smartview/v2/environmental/ibxs/{pathv1}/sensors/{pathv2}/readings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAssets(sorts, source, q, exactMatch, offset, limit, body, callback)</td>
    <td style="padding:15px">Search permissible assets</td>
    <td style="padding:15px">{base_path}/{version}/v1/assets/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetById(assetId, callback)</td>
    <td style="padding:15px">Get details of an asset</td>
    <td style="padding:15px">{base_path}/{version}/v1/assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAttachment(uploadFile, purpose = 'LOA', callback)</td>
    <td style="padding:15px">Upload attachment file</td>
    <td style="padding:15px">{base_path}/{version}/v1/attachments/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachment(attachmentId, callback)</td>
    <td style="padding:15px">Delete attachments by identifier</td>
    <td style="padding:15px">{base_path}/{version}/v1/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrossConnectProviders(crossConnectType = 'STANDARD', ibx, cage, account, connectTo = 'SELF', callback)</td>
    <td style="padding:15px">Get Cross Connect Providers</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnect/providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrossConnectLocations(ibxs, crossConnectType = 'STANDARD', cages, detail, side = 'A_SIDE', asideIbx, accountNumber, callback)</td>
    <td style="padding:15px">Get Cross Connect Locations</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnect/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrossConnectTypes(callback)</td>
    <td style="padding:15px">Get Cross Connect Types</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnect/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrossconnectConnectionServices(ibx, side = 'A_SIDE', asideIbx, providerAccountNo, crossConnectType = 'STANDARD', callback)</td>
    <td style="padding:15px">Get Cross Connect Connection Services</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnect/connectionservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrossConnectPatchPanel(name, ibx, cage, cabinet, legacyName, mediaType, side = 'A_SIDE', asideIbx, providerAccountNo, accountNumber, crossConnectType = 'STANDARD', callback)</td>
    <td style="padding:15px">Get Cross Connect Patch panel</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnect/patchpanel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCrossConnectStandard(action = 'SUBMIT', body, callback)</td>
    <td style="padding:15px">Order Standard Cross Connect</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnect/standard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCrossConnectsStandard(action = 'SUBMIT', body, callback)</td>
    <td style="padding:15px">Order Multiple Cross Connects</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/crossconnects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingAccounts(offset, limit, sorts = 'ACCOUNT_NUMBER', callback)</td>
    <td style="padding:15px">Get billing accounts summary</td>
    <td style="padding:15px">{base_path}/{version}/v1/finance/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadBillingDocument(accountNumber, invoiceId, documentId, callback)</td>
    <td style="padding:15px">Download specific invoice document</td>
    <td style="padding:15px">{base_path}/{version}/v1/finance/accounts/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingAccountByNumber(accountNumber, months, callback)</td>
    <td style="padding:15px">Get specific billing account summary</td>
    <td style="padding:15px">{base_path}/{version}/v1/finance/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchIbxNotifications(offset, limit, sorts, body, callback)</td>
    <td style="padding:15px">Ibx Notifications Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/ibx/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIbxNotification(id, callback)</td>
    <td style="padding:15px">Get Ibx Notification</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/ibx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchNetworkNotifications(offset, limit, sorts, body, callback)</td>
    <td style="padding:15px">Network Notifications Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/network/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNotification(id, callback)</td>
    <td style="padding:15px">Get Network Notification</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRetrieveOrdersLocations(callback)</td>
    <td style="padding:15px">Retrieve order permissible IBX locations</td>
    <td style="padding:15px">{base_path}/{version}/v1/retrieve-orders/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrdersHistory(body, callback)</td>
    <td style="padding:15px">Search Orders History</td>
    <td style="padding:15px">{base_path}/{version}/v1/retrieve-orders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleReport(body, callback)</td>
    <td style="padding:15px">Schedule New Report</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/scheduler?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledReports(offset, limit, sorts, callback)</td>
    <td style="padding:15px">Get your Scheduled Reports</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/scheduler?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduledReports(scheduledIds, callback)</td>
    <td style="padding:15px">Delete your scheduled reports</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/scheduler?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadReports(reportIds, callback)</td>
    <td style="padding:15px">Download list of generated reports</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledReport(scheduledId, callback)</td>
    <td style="padding:15px">Get details of scheduled report</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/scheduler/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScheduledReport(scheduledId, body, callback)</td>
    <td style="padding:15px">Modify scheduled report parameters</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/scheduler/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDefinitions(callback)</td>
    <td style="padding:15px">Get all reports definitions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReports(reportIds, callback)</td>
    <td style="padding:15px">Delete your generated reports</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReports(offset, limit, duration, sorts, statuses, callback)</td>
    <td style="padding:15px">Get your generated reports</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateReport(scheduledId, callback)</td>
    <td style="padding:15px">Generate new report from schedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/scheduler/{pathv1}/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDefinition(reportName, callback)</td>
    <td style="padding:15px">Get report definition by name</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadReport(reportId, callback)</td>
    <td style="padding:15px">Download a generated report</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/{pathv1}/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportById(reportId, callback)</td>
    <td style="padding:15px">Get details of generated report</td>
    <td style="padding:15px">{base_path}/{version}/v1/reportCenter/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">outboundShipmentSubmit(body, callback)</td>
    <td style="padding:15px">Schedule outbound shipment</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/shipment/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocation(detail, ibxs, cages, callback)</td>
    <td style="padding:15px">Get Shipment Locations</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/shipment/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shipmentInbound(body, callback)</td>
    <td style="padding:15px">Schedule inbound shipment</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/shipment/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shipmentPending(body, callback)</td>
    <td style="padding:15px">Schedule pending inbound shipment</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/shipment/pendingStorage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Equipment Install</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/equipmentInstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandShipmentUnpackOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Shipment Unpack</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/shipmentUnpack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandMoveJumperCableOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Move Jumper Cable</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/moveJumperCable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandCageEscortOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Cage Escort</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/cageEscort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandLocatePackageOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Locate Package</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/locatePackage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandPicturesDocumentOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Pictures Document</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/picturesDocument?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandPatchCableInstallOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Patch Cable Install</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/patchCableInstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandPatchCableRemovalOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Patch Cable Removal</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/patchCableRemoval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandCageCleanupOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Cage Cleanup</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/cageCleanup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandCableRequestOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Cable Request</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/cableRequest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandRunJumperCableOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Run Jumper Cable</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/runJumperCable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleSmartHandOthersOrder(body, callback)</td>
    <td style="padding:15px">Order Smarthands Other</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/other?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartHandTypes(callback)</td>
    <td style="padding:15px">Get Smart Hands Types</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/smarthands/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTroubleTicketType(category = 'Cross Connect', callback)</td>
    <td style="padding:15px">Get Trouble Ticket Types</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/troubleticket/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">placeTroubleTicketOrder(body, callback)</td>
    <td style="padding:15px">Order Trouble ticket</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/troubleticket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchUser(q, filters, searchType = 'CONTAINS', statuses, favoriteOnly, forOrg, permissions, permissibleIbxs, permissibleCages, roles, offset, limit, sorts, isOnboarded, hasPortalAccess, isManageable, excludeSelf, callback)</td>
    <td style="padding:15px">Search Users in Organization</td>
    <td style="padding:15px">{base_path}/{version}/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workVisitOrder(body, callback)</td>
    <td style="padding:15px">Schedule Work Visit</td>
    <td style="padding:15px">{base_path}/{version}/v1/orders/workvisit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
